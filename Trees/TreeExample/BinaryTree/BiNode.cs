﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TreeExample
{
    public class BiNode<T>
    {
        public BiNode<T> LeftNode { get; set; }
        public BiNode<T> RightNode { get; set; }
        public NodeData<T> NodeData { get; set; }

        public BiNode(NodeData<T> data)
        {
            NodeData = data;
        }
    }
}
