﻿using System.Collections.Generic;

namespace TreeExample
{
    public class BinarySearchTree<T>
    {
        public BiNode<T> Root { get; private set; }

        public BinarySearchTree()
        {

        }

        public BinarySearchTree(BiNode<T> root)
        {
            Root = root;
        }

        public bool IsValidBinarySearchTree()
        {
            // an inorder traversal of a binary search tree should return a list in acsending order.
            // if not it is not a valid binary search tree
            DepthFirstSearch dfs = new DepthFirstSearch();

            NodeData<T> previous = null;
            foreach (NodeData<T> item in dfs.InOrderTraversal<T>(Root))
            {
                if (previous == null)
                {
                    previous = item;
                }
                else
                {
                    if (previous.Id >= item.Id)
                    {
                        return false;
                    }
                    previous = item;
                }
            }
            return true;
        }

        public NodeData<T> Search_Iterative(int id)
        {
            BiNode<T> node = Root;

            while(node != null)
            {
                if(id == node.NodeData.Id)
                {
                    return node.NodeData;
                }
                else if(id < node.NodeData.Id)
                {
                    node = node.LeftNode;
                }
                else if(id > node.NodeData.Id)
                {
                    node = node.RightNode;
                }
            }
            return null;
        }

        public NodeData<T> Search_Recursive(int id)
        {
            return Search_Recursive(Root, id);
        }

        private NodeData<T> Search_Recursive(BiNode<T> node, int id)
        {
            if(id == node.NodeData.Id)
            {
                return node.NodeData;
            }
            else if(id < node.NodeData.Id)
            {
                return Search_Recursive(node.LeftNode, id);
            }
            else if(id > node.NodeData.Id)
            {
                return Search_Recursive(node.RightNode, id);
            }

            return null;
        }

        public NodeData<T> Min_Iterative()
        {
            BiNode<T> node = Root;

            while(node.LeftNode != null)
            {
                node = node.LeftNode;
            }
            return node.NodeData;
        }

        public NodeData<T> Min_Recursive()
        {
            return Min_Recursive(Root);
        }

        private NodeData<T> Min_Recursive(BiNode<T> node)
        {
            if(node.LeftNode == null)
            {
                return node.NodeData;
            }
            else
            {
                return Min_Recursive(node.LeftNode);
            }
        }

        public NodeData<T> Max_Iterative()
        {
            BiNode<T> node = Root;

            while(node.RightNode != null)
            {
                node = node.RightNode;
            }
            return node.NodeData;
        }

        public NodeData<T> Max_Recursive()
        {
            return Max_Recursive(Root);
        }

        private NodeData<T> Max_Recursive(BiNode<T> node)
        {
            if(node.RightNode == null)
            {
                return node.NodeData;
            }
            else
            {
                return Max_Recursive(node.RightNode);
            }
        }

        public void Insert_Iterative(BiNode<T> nodeToInsert)
        {
            if(Root == null)
            {
                Root = nodeToInsert;
                return;
            }

            BiNode<T> currentNode = Root;

            while(currentNode != null)
            {
                if(currentNode.NodeData.Id == nodeToInsert.NodeData.Id)
                {
                    throw new System.Exception("Duplicate id");
                }
                else if(nodeToInsert.NodeData.Id < currentNode.NodeData.Id)
                {
                    if(currentNode.LeftNode == null)
                    {
                        currentNode.LeftNode = nodeToInsert;
                        return;
                    }
                    else
                    {
                        currentNode = currentNode.LeftNode;
                    }
                }
                else if(nodeToInsert.NodeData.Id > currentNode.NodeData.Id)
                {
                    if(currentNode.RightNode == null)
                    {
                        currentNode.RightNode = nodeToInsert;
                        return;
                    }
                    else
                    {
                        currentNode = currentNode.RightNode;
                    }
                }
            }
        }

        public void Insert_Recursive(BiNode<T> nodeToInsert)
        {
            if(Root == null)
            {
                Root = nodeToInsert;
                return;
            }

            Insert_Recursive(Root, nodeToInsert);
        }

        private void Insert_Recursive(BiNode<T> currentNode, BiNode<T> nodeToInsert)
        {
            if(nodeToInsert.NodeData.Id == currentNode.NodeData.Id)
            {
                throw new System.Exception("Duplicate id");
            }
            else if(nodeToInsert.NodeData.Id < currentNode.NodeData.Id)
            {
                if(currentNode.LeftNode == null)
                {
                    currentNode.LeftNode = nodeToInsert;
                }
                else
                {
                    Insert_Recursive(currentNode.LeftNode, nodeToInsert);
                }
            }
            else if(nodeToInsert.NodeData.Id > currentNode.NodeData.Id)
            {
                if(currentNode.RightNode == null)
                {
                    currentNode.RightNode = nodeToInsert;
                }
                else
                {
                    Insert_Recursive(currentNode.RightNode, nodeToInsert);
                }
            }
        }

        public void Delete_Iterative(int id)
        {
            BiNode<T> currentNode = Root;
            BiNode<T> parentNode = null;
            while(currentNode != null)
            {
                if(currentNode.NodeData.Id == id)
                {
                    break;
                }
                parentNode = currentNode;
                if(id < currentNode.NodeData.Id)
                {
                    currentNode = currentNode.LeftNode;
                }
                else
                {
                    currentNode = currentNode.RightNode;
                }
            }

            if(currentNode == null)
            {
                return; // Id Not found
            }

            // Find in order successor
            if(currentNode.LeftNode != null && currentNode.RightNode != null)
            {
                BiNode<T> parentSuccessorNode = currentNode;
                BiNode<T> successorNode = currentNode.RightNode;

                while(successorNode.LeftNode != null)
                {
                    parentSuccessorNode = successorNode;
                    successorNode = successorNode.LeftNode;
                }

                currentNode = successorNode;
                parentNode = parentSuccessorNode;
            }

            // delete
            BiNode<T> newChild = currentNode.LeftNode ?? currentNode.RightNode;

            if(parentNode == null)
            {
                Root = newChild;
            }
            else if(currentNode.NodeData.Id == parentNode.LeftNode.NodeData.Id)
            {
                parentNode.LeftNode = newChild;
            }
            else
            {
                parentNode.RightNode = newChild;
            }
        }

        public void Delete_Recursive(int id)
        {
            Delete_Recursive(Root, id);
        }

        private BiNode<T> Delete_Recursive(BiNode<T> currentNode, int id)
        {
            if(currentNode == null)
            {
                return currentNode;
            }

            if(id < currentNode.NodeData.Id)
            {
                currentNode.LeftNode = Delete_Recursive(currentNode.LeftNode, id);
            }
            else if(id > currentNode.NodeData.Id)
            {
                currentNode.RightNode = Delete_Recursive(currentNode.RightNode, id);
            }
            else
            {
                if(currentNode.LeftNode != null & currentNode.RightNode != null)
                {
                    BiNode<T> successorNode = currentNode.RightNode;
                    while(successorNode != null)
                    {
                        successorNode = successorNode.LeftNode;
                    }
                    currentNode.RightNode = Delete_Recursive(currentNode.RightNode, id);
                }
                else
                {
                    if(currentNode.LeftNode != null)
                    {
                        currentNode = currentNode.LeftNode;
                    }
                    else if(currentNode.RightNode != null)
                    {
                        currentNode = currentNode.RightNode;
                    }
                    
                }
            }

            return currentNode;
        }
    }


}
