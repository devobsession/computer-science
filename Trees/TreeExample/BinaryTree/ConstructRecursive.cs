﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TreeExample
{
    public class ConstructRecursive<T>
    {

        public BiNode<T> ConstructBinaryTree_Preorder_Inorder(List<NodeData<T>> preorderElements, List<NodeData<T>> inorderElements)
        {
            if(preorderElements == null || preorderElements.Count == 0)
            {
                return null;
            }
            NodeData<T> root = preorderElements[0];

            int centerIndex = inorderElements.IndexOf(root);
            List<NodeData<T>> leftSubTreeInorder = inorderElements.GetRange(0, centerIndex);
            List<NodeData<T>> rightSubTreeInorder = inorderElements.GetRange(centerIndex + 1, inorderElements.Count - (centerIndex + 1));

            List<NodeData<T>> leftSubTreePreorder = new List<NodeData<T>>();
            List<NodeData<T>> rightSubTreePreorder = new List<NodeData<T>>();
            foreach(NodeData<T> preorderElement in preorderElements)
            {
                if(leftSubTreeInorder.Contains(preorderElement))
                {
                    leftSubTreePreorder.Add(preorderElement);
                }
                else if(rightSubTreeInorder.Contains(preorderElement))
                {
                    rightSubTreePreorder.Add(preorderElement);
                }
            }

            return new BiNode<T>(root)
            {
                LeftNode = ConstructBinaryTree_Preorder_Inorder(leftSubTreePreorder, leftSubTreeInorder),
                RightNode = ConstructBinaryTree_Preorder_Inorder(rightSubTreePreorder, rightSubTreeInorder)
            };
        }

        public BiNode<T> ConstructBinaryTree_Postorder_Inorder(List<NodeData<T>> postorderElements, List<NodeData<T>> inorderElements)
        {
            if(postorderElements == null || postorderElements.Count == 0)
            {
                return null;
            }
            NodeData<T> root = postorderElements[postorderElements.Count-1];

            int centerIndex = inorderElements.IndexOf(root);
            List<NodeData<T>> leftSubTreeInorder = inorderElements.GetRange(0, centerIndex);
            List<NodeData<T>> rightSubTreeInorder = inorderElements.GetRange(centerIndex + 1, inorderElements.Count - (centerIndex + 1));

            List<NodeData<T>> leftSubTreePostorder = new List<NodeData<T>>();
            List<NodeData<T>> rightSubTreePostorder = new List<NodeData<T>>();
            foreach(NodeData<T> postorderElement in postorderElements)
            {
                if(leftSubTreeInorder.Contains(postorderElement))
                {
                    leftSubTreePostorder.Add(postorderElement);
                }
                else if(rightSubTreeInorder.Contains(postorderElement))
                {
                    rightSubTreePostorder.Add(postorderElement);
                }
            }

            return new BiNode<T>(root)
            {
                LeftNode = ConstructBinaryTree_Postorder_Inorder(leftSubTreePostorder, leftSubTreeInorder),
                RightNode = ConstructBinaryTree_Postorder_Inorder(rightSubTreePostorder, rightSubTreeInorder)
            };
        }


    }
}
