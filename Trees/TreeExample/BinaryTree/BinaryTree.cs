﻿using System.Collections.Generic;

namespace TreeExample
{
    public class BinaryTree<T>
    {
        public BiNode<T> Root { get; private set; }
        public DepthFirstSearch DepthFirstSearch { get; set; }
        public BinaryTree(BiNode<T> root)
        {
            DepthFirstSearch = new DepthFirstSearch();
            Root = root;
        }

        public BinaryTree(List<NodeData<T>> inorderElements, List<NodeData<T>> orderedElements, bool isPreorder)
        {
            DepthFirstSearch = new DepthFirstSearch();
            if (isPreorder)
            {
                Root = new ConstructRecursive<T>().ConstructBinaryTree_Preorder_Inorder(orderedElements, inorderElements);
            }
            else
            {
                Root = new ConstructRecursive<T>().ConstructBinaryTree_Postorder_Inorder(orderedElements, inorderElements);
            }
        }
                
        public int Height_Recursive()
        {
            return Height_Recursive(Root);
        }

        private int Height_Recursive(BiNode<T> node)
        {
            if(node == null)
            {
                return 0;
            }

            int leftHeight = Height_Recursive(node.LeftNode);
            int rightHeight = Height_Recursive(node.RightNode);

            return 1 + (leftHeight > rightHeight ? leftHeight : rightHeight);
        }

    }


}
