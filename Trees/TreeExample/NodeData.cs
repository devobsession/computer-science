﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TreeExample
{
    public class NodeData<T>
    {
        public int Id { get; set; }
        public T Value { get; set; }

        public NodeData(int id, T value)
        {
            Value = value;
            Id = id;
        }
    }
}
