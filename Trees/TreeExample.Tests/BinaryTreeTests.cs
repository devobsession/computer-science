using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using TreeExample;

namespace Tests
{
    public class BinaryTreeTests
    {
        private readonly int[] _correctInorder = new int[] { 2, 1, 3, 0, 5, 4, 6 };
        private readonly int[] _correctPreorder = new int[] { 0, 1, 2, 3, 4, 5, 6 };
        private readonly int[] _correctPostorder = new int[] { 2, 3, 1, 5, 6, 4, 0 };
        private readonly int[] _correctLevelorder = new int[] { 0, 1, 4, 2, 3, 5, 6 };

        BinaryTree<string> _binaryTree;
        BinaryTree<string> _complexBinaryTree;

        [SetUp]
        public void SetUp()
        {
            BiNode<string> root = new BiNode<string>(new NodeData<string>(0, "a"))
            {
                LeftNode = new BiNode<string>(new NodeData<string>(1, "b"))
                {
                    LeftNode = new BiNode<string>(new NodeData<string>(2, "c")),
                    RightNode = new BiNode<string>(new NodeData<string>(3, "d"))
                },
                RightNode = new BiNode<string>(new NodeData<string>(4, "e"))
                {
                    LeftNode = new BiNode<string>(new NodeData<string>(5, "f")),
                    RightNode = new BiNode<string>(new NodeData<string>(6, "g"))
                }
            };

            _binaryTree = new BinaryTree<string>(root);


            BiNode<string> complexRoot = new BiNode<string>(new NodeData<string>(0, "a"))
            {
                LeftNode = new BiNode<string>(new NodeData<string>(1, "b"))
                {
                    LeftNode = new BiNode<string>(new NodeData<string>(2, "c"))
                    {
                        RightNode = new BiNode<string>(new NodeData<string>(21, "cb"))
                    },
                    RightNode = new BiNode<string>(new NodeData<string>(3, "d"))
                    {
                        RightNode = new BiNode<string>(new NodeData<string>(31, "db"))
                    },
                },
                RightNode = new BiNode<string>(new NodeData<string>(4, "e"))
                {
                    LeftNode = new BiNode<string>(new NodeData<string>(5, "f"))
                    {
                        LeftNode = new BiNode<string>(new NodeData<string>(50, "fa"))
                    },
                    RightNode = new BiNode<string>(new NodeData<string>(6, "g"))
                    {
                        LeftNode = new BiNode<string>(new NodeData<string>(61, "gb"))
                    },
                }
            };

            _complexBinaryTree = new BinaryTree<string>(complexRoot);
        }
        
        [Test]
        public void Construct_BinaryTree_From_Preorder_Inorder()
        {
            // Get preorder and inorder
            List<NodeData<string>> preorderResults = _binaryTree.DepthFirstSearch.PreOrderTraversal(_binaryTree.Root).ToList();
            List<NodeData<string>> inorderResults = _binaryTree.DepthFirstSearch.InOrderTraversal(_binaryTree.Root).ToList();

            // construct tree
            BinaryTree<string> constructedTree = new BinaryTree<string>(inorderResults, preorderResults, true);

            // check preorder of newly constructed tree
            int index = 0;
            IEnumerable<NodeData<string>> treeResults = constructedTree.DepthFirstSearch.PreOrderTraversal(_binaryTree.Root);
            foreach(NodeData<string> result in treeResults)
            {
                Assert.AreEqual(_correctPreorder[index], result.Id);
                index++;
            }
        }

        [Test]
        public void Construct_BinaryTree_From_Postorder_Inorder()
        {
            // Get preorder and inorder
            List<NodeData<string>> postorderResults = _binaryTree.DepthFirstSearch.PostOrderTraversal(_binaryTree.Root).ToList();
            List<NodeData<string>> inorderResults = _binaryTree.DepthFirstSearch.InOrderTraversal(_binaryTree.Root).ToList();

            // construct tree
            BinaryTree<string> constructedTree = new BinaryTree<string>(inorderResults, postorderResults, false);

            // check preorder of newly constructed tree
            int index = 0;
            IEnumerable<NodeData<string>> treeResults = constructedTree.DepthFirstSearch.PreOrderTraversal(_binaryTree.Root);
            foreach(NodeData<string> result in treeResults)
            {
                Assert.AreEqual(_correctPreorder[index], result.Id);
                index++;
            }
        }

        [Test]
        public void Height_Recursive_Correct()
        {
            int height = _binaryTree.Height_Recursive();
            Assert.AreEqual(3, height);
        }        

    }
}