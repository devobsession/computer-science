using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using TreeExample;

namespace Tests
{
    public class BinarySearchTreeTests
    {

        BinarySearchTree<string> _binarySearchTree;

        [SetUp]
        public void SetUp()
        {
            BiNode<string> root = new BiNode<string>(new NodeData<string>(5, "a"))
            {
                LeftNode = new BiNode<string>(new NodeData<string>(3, "b"))
                {
                    LeftNode = new BiNode<string>(new NodeData<string>(2, "c")),
                    RightNode = new BiNode<string>(new NodeData<string>(4, "d"))
                },
                RightNode = new BiNode<string>(new NodeData<string>(7, "e"))
                {
                    LeftNode = new BiNode<string>(new NodeData<string>(6, "f")),
                    RightNode = new BiNode<string>(new NodeData<string>(8, "g"))
                }
            };

            _binarySearchTree = new BinarySearchTree<string>(root);
        }

        [Test]
        public void Search_Tree_Is_Valid()
        {
            bool isValid = _binarySearchTree.IsValidBinarySearchTree();
            Assert.IsTrue(isValid);
        }

        [Test]
        public void Search_Tree_Is_Not_Valid()
        {
            BiNode<string> root = new BiNode<string>(new NodeData<string>(5, "a"))
            {
                LeftNode = new BiNode<string>(new NodeData<string>(3, "b"))
                {
                    LeftNode = new BiNode<string>(new NodeData<string>(2, "c")),
                    RightNode = new BiNode<string>(new NodeData<string>(10, "d"))
                },
                RightNode = new BiNode<string>(new NodeData<string>(7, "e"))
                {
                    LeftNode = new BiNode<string>(new NodeData<string>(6, "f")),
                    RightNode = new BiNode<string>(new NodeData<string>(8, "g"))
                }
            };
            ;

            var badBinarySearchTree = new BinarySearchTree<string>(root);

            bool isValid = badBinarySearchTree.IsValidBinarySearchTree();
            Assert.IsFalse(isValid);
        }

        [Test]
        public void Search_Tree_Iterative()
        {
            int id = 6;
            var result = _binarySearchTree.Search_Iterative(id);

            Assert.AreEqual(id, result.Id);
        }

        [Test]
        public void Search_Tree_Recursive()
        {
            int id = 6;
            var result = _binarySearchTree.Search_Recursive(id);

            Assert.AreEqual(id, result.Id);
        }

        [Test]
        public void Get_Min_Iterative()
        {
            var result = _binarySearchTree.Min_Iterative();

            Assert.AreEqual(2, result.Id);
        }

        [Test]
        public void Get_Min_Recursive()
        {
            var result = _binarySearchTree.Min_Recursive();

            Assert.AreEqual(2, result.Id);
        }
        
        [Test]
        public void Get_Max_Iterative()
        {
            var result = _binarySearchTree.Max_Iterative();

            Assert.AreEqual(8, result.Id);
        }

        [Test]
        public void Get_Max_Recursive()
        {
            var result = _binarySearchTree.Max_Recursive();

            Assert.AreEqual(8, result.Id);
        }
        
        [Test]
        public void Insert_Iterative()
        {
            int id = 10;
           _binarySearchTree.Insert_Iterative(new BiNode<string>(new NodeData<string>(id, "inserted")));
            Assert.AreEqual(id, _binarySearchTree.Max_Iterative().Id);
        }

        [Test]
        public void Insert_Recursive()
        {
            int id = 10;
            _binarySearchTree.Insert_Recursive(new BiNode<string>(new NodeData<string>(id, "inserted")));
            Assert.AreEqual(id, _binarySearchTree.Max_Recursive().Id);
        }

    }
}