using NUnit.Framework;
using QueueExample;

namespace Tests
{
    public class CustomDoubleQueueTests
    {
        [Test]
        public void InputFrontAndBack_Then_Dequeue()
        {
            OutputRestrictedDoubleQueue<int> queue = new OutputRestrictedDoubleQueue<int>();
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.OfferFirst(3);
            // 3, 1, 2

            var item = queue.Dequeue();
            Assert.AreEqual(3, item);
        }

        [Test]
        public void Enqueue_Then_OutputFrontAndBack()
        {
            InputRestrictedDoubleQueue<int> queue = new InputRestrictedDoubleQueue<int>();
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);
            // 1, 2, 3
            int item = queue.Dequeue();
            Assert.AreEqual(1, item);

            item = queue.Pop();
            Assert.AreEqual(3, item);
        }

        [Test]
        public void InputFrontAndBack_Then_OutputFrontAndBack()
        {
            NoRestrictionsDoubleQueue<int> queue = new NoRestrictionsDoubleQueue<int>();
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);
            queue.Push(4);
            queue.Push(5);
            queue.Push(6);
            // 6, 5, 4, 1, 2, 3

            int item = queue.Dequeue();
            Assert.AreEqual(6, item);

            item = queue.Pop();
            Assert.AreEqual(3, item);
        }
    }
}