using NUnit.Framework;
using System.Collections.Generic;

namespace Tests
{
    public class BuiltInQueueTests
    {
        [Test]
        public void Dequeue_Front_Of_Queue()
        {
            Queue<int> queue = new Queue<int>();
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);

            int item = queue.Dequeue();
            Assert.AreEqual(1, item);

            item = queue.Dequeue();
            Assert.AreEqual(2, item);
        }

        [Test]
        public void Peek_Front_Of_Queue()
        {
            Queue<int> queue = new Queue<int>();
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);

            int item = queue.Peek();
            Assert.AreEqual(1, item);

            item = queue.Peek();
            Assert.AreEqual(1, item);
        }

    }
}