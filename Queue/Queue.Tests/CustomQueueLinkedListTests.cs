using NUnit.Framework;
using QueueExample;

namespace Tests
{
    public class CustomQueueLinkedListTests
    {
        [Test]
        public void Dequeue_Front_Of_Queue()
        {
            QueueLinkedListCustom<int> queue = new QueueLinkedListCustom<int>();
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);
            int item = queue.Dequeue();
            Assert.AreEqual(1, item);

            item = queue.Dequeue();
            Assert.AreEqual(2, item);
        }

        [Test]
        public void Peek_Front_Of_Queue()
        {
            QueueLinkedListCustom<int> queue = new QueueLinkedListCustom<int>();
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);

            int item = queue.Peek();
            Assert.AreEqual(1, item);

            item = queue.Peek();
            Assert.AreEqual(1, item);
        }

        [Test]
        public void Underflow()
        {
            Assert.Throws(typeof(System.InvalidOperationException), () => {
                QueueLinkedListCustom<int> queue = new QueueLinkedListCustom<int>();
                int item = queue.Dequeue();
            });
        }

        [Test]
        public void Dequeue_Until_Empty_Without_Exception()
        {
            QueueLinkedListCustom<int> queue = new QueueLinkedListCustom<int>();
            queue.Enqueue(1);
            queue.Enqueue(2);

            int item = queue.Dequeue();
            item = queue.Dequeue();
            Assert.AreEqual(0, queue.Count());
        }
    }
}