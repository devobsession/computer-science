using NUnit.Framework;
using QueueExample;

namespace Tests
{
    public class CustomQueueArrayTests
    {
        [Test]
        public void Dequeue_Front_Of_Queue()
        {
            QueueArrayCustom<int> queue = new QueueArrayCustom<int>();
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);

            int item = queue.Dequeue();
            Assert.AreEqual(1, item);

            item = queue.Dequeue();
            Assert.AreEqual(2, item);
        }

        [Test]
        public void Peek_Front_Of_Queue()
        {
            QueueArrayCustom<int> queue = new QueueArrayCustom<int>();
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);

            int item = queue.Peek();
            Assert.AreEqual(1, item);

            item = queue.Peek();
            Assert.AreEqual(1, item);
        }

        [Test]
        public void Underflow()
        {
            Assert.Throws(typeof(System.InvalidOperationException), () => {
                QueueArrayCustom<int> queue = new QueueArrayCustom<int>();
                int item = queue.Dequeue();
            });
        }

        [Test]
        public void OverFlow()
        {
            Assert.Throws(typeof(System.InvalidOperationException), () => {
                QueueArrayCustom<int> queue = new QueueArrayCustom<int>(1);
                queue.Enqueue(1);
                queue.Enqueue(2);
                queue.Enqueue(3);
            });
        }

        [Test]
        public void Dequeue_Until_Empty_Without_Exception()
        {
            QueueArrayCustom<int> queue = new QueueArrayCustom<int>();
            queue.Enqueue(1);
            queue.Enqueue(2);

            int item = queue.Dequeue();
            item = queue.Dequeue();
            Assert.AreEqual(0, queue.Count());
        }

        [Test]
        public void Count()
        {
            QueueArrayCustom<int> queue = new QueueArrayCustom<int>();
            queue.Enqueue(1);
            queue.Enqueue(2);
            Assert.AreEqual(2, queue.Count());
        }

        [Test]
        public void Count_Zero()
        {
            QueueArrayCustom<int> queue = new QueueArrayCustom<int>();
            Assert.AreEqual(0, queue.Count());
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Dequeue();
            queue.Dequeue();
            Assert.AreEqual(0, queue.Count());
        }

        [Test]
        public void Count_After_Circle()
        {
            QueueArrayCustom<int> queue = new QueueArrayCustom<int>(5);
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);
            queue.Enqueue(4);
            queue.Enqueue(5);           
            Assert.AreEqual(5, queue.Count());

            queue.Dequeue();
            queue.Dequeue();
            queue.Enqueue(1);

            Assert.AreEqual(4, queue.Count());
        }

    }
}