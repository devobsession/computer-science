
namespace QueueExample
{
    public class QueueLinkedListCustom<T>
    {
        private Node<T> _rear;
        private Node<T> _front
        {
            get
            {
                return _rear.Next ?? _rear;
            }
            set
            {
                _rear.Next = value;
            }
        }

        public void Enqueue(T item)
        {
            Node<T> newNode = new Node<T>(item);
            if(IsEmpty())
            {
                _rear = newNode;
                _front = _rear;
            }
            else
            {
                newNode.Next = _front;
                _rear.Next = newNode;
                _rear = newNode;
            }
        }

        public T Dequeue()
        {
            if(IsEmpty())
            {
                throw new System.InvalidOperationException("Queue is Empty");
            }

            Node<T> temp = _front;
            if(_rear == _front)
            {
                _rear = null;
            }
            else
            {
                _rear.Next = temp.Next;
            }
            
            return temp.Value;
        }

        public T Peek()
        {
            return _front.Value;
        }

        public bool IsEmpty()
        {
            return _rear == null;
        }

        public int Count()
        {
            if(_rear == null)
            {
                return 0;
            }
            
            var temp = _front;
            int count = 1;
            while(temp != _rear)
            {
                count++;
                temp = temp.Next;
            }
            return count;
        }

    }
}