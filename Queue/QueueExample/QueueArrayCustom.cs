

using System;

namespace QueueExample
{
    public class QueueArrayCustom<T>
    {
        private T[] _queueArr;
        private int _front;
        private int _back;

        public QueueArrayCustom()
        {
            _front = -1;
            _back = 0;
            _queueArr = new T[10];
        }

        public QueueArrayCustom(int size)
        {
            _front = -1;
            _back = 0;
            _queueArr = new T[size];
        }

        public void Enqueue(T item)
        {
            if(_back == _front)
            {
                throw new InvalidOperationException("Queue overflow");
            }

            _queueArr[_back] = item;

            if(_front == -1)
            {
                _front = _back;
            }

            if(_back == _queueArr.Length - 1)
            {
                _back = 0;
            }
            else
            {
                _back++;
            }            
        }

        public T Dequeue()
        {
            if(IsEmpty())
            {
                throw new InvalidOperationException("Queue underflow");
            }

            T frontItem = _queueArr[_front];

           
            if(_front == _queueArr.Length - 1)
            {
                _front = 0;
            }
            else
            {
                _front++;
            }

            if(_front == _back)
            {
                _front = -1;
            }

            return frontItem;
        }

        public T Peek()
        {
            return _queueArr[_front];
        }

        public bool IsEmpty()
        {
           return _front == -1;
        }

        public int Count()
        {
            if(IsEmpty())
            {
                return 0;
            }

            if(_front < _back)
            {
                return _back - _front;
            }
            else
            {
                return ((_queueArr.Length) - _front) + _back;
            }
        }
    }
}