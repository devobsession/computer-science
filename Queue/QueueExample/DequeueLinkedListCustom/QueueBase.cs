
namespace QueueExample
{
    /// <summary>
    /// Double ended queue
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class QueueBase<T>
    {
        protected Node<T> _rear;
        protected Node<T> _front;

        public void Enqueue(T item)
        {
            OfferLast(item);
        }

        public void OfferLast(T item)
        {
            Node<T> newNode = new Node<T>(item);
            if (IsEmpty())
            {
                _rear = newNode;
                _front = newNode;
            }
            else
            {
                _rear.Next = newNode;
                _rear = newNode;
            }
        }

        public T Dequeue()
        {
            return PollFirst();
        }

        public T PollFirst()
        {
            if (IsEmpty())
            {
                throw new System.InvalidOperationException("Queue is Empty");
            }

            Node<T> temp = _front;
            if (_rear == _front)
            {
                _rear = null;
                _front = null;
            }
            else
            {
                _front = temp.Next;
            }

            return temp.Value;
        }
                
        public bool IsEmpty()
        {
            return _rear == null;
        }

        public int Count()
        {
            if(_rear == null)
            {
                return 0;
            }
            
            var temp = _front;
            int count = 1;
            while(temp != _rear)
            {
                count++;
                temp = temp.Next;
            }
            return count;
        }

    }
}