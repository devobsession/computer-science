
namespace QueueExample
{
    /// <summary>
    /// Insertion restricted to one end (in this case front)
    /// Deletion allowed at both ends
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class InputRestrictedDoubleQueue<T> : QueueBase<T>
    {
        public T Pop()
        {
            return PollLast();
        }

        public T PollLast()
        {
            if (IsEmpty())
            {
                throw new System.InvalidOperationException("Queue is Empty");
            }

            Node<T> lastNode = null;
            if (_front.Next == null)
            {
                lastNode = _front;
                _front = null;
                return lastNode.Value;
            }

            Node<T> currentNode = _front;
            while (currentNode.Next.Next != null)
            {
                currentNode = currentNode.Next;
            }

            lastNode = currentNode.Next;
            _rear = currentNode;
            currentNode.Next = null;

            return lastNode.Value;
        }

    }
}