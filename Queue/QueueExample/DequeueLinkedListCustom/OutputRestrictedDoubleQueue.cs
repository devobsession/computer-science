
namespace QueueExample
{
    /// <summary>
    /// Insertion allowed at both ends 
    /// Deletion restricted to one end (in this case back)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class OutputRestrictedDoubleQueue<T> : QueueBase<T>
    {
        public void Push(T item)
        {
            OfferFirst(item);
        }

        public void OfferFirst(T item)
        {
            Node<T> newNode = new Node<T>(item);
            if (IsEmpty())
            {
                _rear = newNode;
                _front = newNode;
            }
            else
            {
                newNode.Next = _front;
                _front = newNode;
            }
        }

    }
}