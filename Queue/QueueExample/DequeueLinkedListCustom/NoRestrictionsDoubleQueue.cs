
namespace QueueExample
{
    /// <summary>
    /// Insertion restricted to one end (in this case front)
    /// Deletion allowed at both ends
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class NoRestrictionsDoubleQueue<T> : QueueBase<T>
    {

        public T Pop()
        {
            return PollLast();
        }

        public T PollLast()
        {
            if (IsEmpty())
            {
                throw new System.InvalidOperationException("Queue is Empty");
            }

            Node<T> lastNode = null;
            if (_front.Next == null)
            {
                lastNode = _front;
                _front = null;
                return lastNode.Value;
            }

            Node<T> currentNode = _front;
            while (currentNode.Next.Next != null)
            {
                currentNode = currentNode.Next;
            }

            lastNode = currentNode.Next;
            _rear = currentNode;
            currentNode.Next = null;

            return lastNode.Value;
        }

        public void Push(T item)
        {
            OfferFirst(item);
        }

        public void OfferFirst(T item)
        {
            Node<T> newNode = new Node<T>(item);
            if (IsEmpty())
            {
                _rear = newNode;
                _front = newNode;
            }
            else
            {
                newNode.Next = _front;
                _front = newNode;
            }
        }

    }
}