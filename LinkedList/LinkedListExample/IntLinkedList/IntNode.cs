﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinkedListExample
{
    [DebuggerDisplay("{Value}")]
    public class IntNode
    {
        public int Value { get; set; }

        public IntNode Next { get; set; }

        public IntNode()
        {
        }
    }
}
