﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LinkedListExample
{
    public class IntLinkedList
    {
        private IntNode _startNode;

        public IntLinkedList(int[] nodeValues)
        {
            _startNode = new IntNode()
            {
                Value = nodeValues.First()
            };
            IntNode previousNode = _startNode;

            for (int i = 1; i < nodeValues.Length; i++)
            {
                previousNode.Next = new IntNode()
                {
                    Value = nodeValues[i]
                };
                previousNode = previousNode.Next;
            }
        }

        public IntNode GetFirstNode()
        {
            return _startNode;
        }

        public IEnumerable<int> GetValues()
        {
            for (IntNode currentNode = _startNode; currentNode != null; currentNode = currentNode.Next)
            {
                yield return currentNode.Value;
            }
        }

        public void SortByLinkSwap()
        {
            IntNode previous = null;
            IntNode pointer1;
            IntNode pointer2;
            IntNode end;

            for (end = null; _startNode.Next != end; end = pointer1)
            {
                for (pointer1 = _startNode; pointer1.Next != end; pointer1 = pointer1.Next)
                {
                    pointer2 = pointer1.Next;
                    if (pointer1.Value > pointer2.Value)
                    {
                        pointer1.Next = pointer2.Next;
                        pointer2.Next = pointer1;

                        if (pointer1 == _startNode)
                        {
                            _startNode = pointer2;
                        }
                        else
                        {
                            previous.Next = pointer2;
                        }

                        var temp = pointer1;
                        pointer1 = pointer2;
                        pointer2 = temp;
                    }

                    previous = pointer1;
                }
            }
        }

        public void SortByValueSwap()
        {
            IntNode pointer1;
            IntNode pointer2;
            IntNode end;

            for (end = null; _startNode.Next != end; end = pointer1)
            {
                for (pointer1 = _startNode; pointer1.Next != end; pointer1 = pointer1.Next)
                {
                    pointer2 = pointer1.Next;
                    if (pointer1.Value > pointer2.Value)
                    {
                        int temp = pointer1.Value;
                        pointer1.Value = pointer2.Value;
                        pointer2.Value = temp;
                    }
                }
            }
        }

        public void SortAndMerge(IntLinkedList otherList)
        {
            SortByValueSwap();
            otherList.SortByValueSwap();

            IntNode end = null; 
            IntNode pointer1 = _startNode;
            IntNode pointer2 = otherList._startNode;
          
            if (pointer1.Value <= pointer2.Value)
            {
                _startNode = pointer1;
                end = pointer1;
                pointer1 = pointer1.Next;
            }
            else
            {
                _startNode = pointer2;
                end = pointer2;
                pointer2 = pointer2.Next;
            }

            while (pointer1 != null && pointer2 != null)
            {
                if (pointer1.Value <= pointer2.Value)
                {
                    end.Next = pointer1;
                    pointer1 = pointer1.Next;
                    end = end.Next;
                }
                else
                {
                    end.Next = pointer2;
                    pointer2 = pointer2.Next;
                    end = end.Next;
                }
            }

            if (pointer1 != null)
            {
                end.Next = pointer1;
            }

            if (pointer2 != null)
            {
                end.Next = pointer2;
            }
        }

    }
}
