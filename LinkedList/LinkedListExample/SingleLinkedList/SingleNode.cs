﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LinkedListExample
{
    [DebuggerDisplay("{Value}")]
    public class SingleNode<T>
    {
        public T Value { get; set; }

        public SingleNode<T> Next { get; set; }

        public SingleNode()
        {
        }

        public SingleNode(T value)
        {
            Value = value;
        }
    }
}
