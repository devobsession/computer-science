﻿using System;

namespace LinkedListExample
{
    public class SingleLinkedListCustom<T>
    {
        private SingleNode<T> _startNode;

        public void AddAfter(SingleNode<T> node, T value)
        {
            SingleNode<T> newNode = new SingleNode<T>(value);
            newNode.Next = node.Next;
            node.Next = newNode;
        }

        public void AddBefore(SingleNode<T> node, T value)
        {
            if (_startNode == null)
            {
                throw new Exception();
            }

            SingleNode<T> newNode = new SingleNode<T>(value);

            if (_startNode == node)
            {
                newNode.Next = _startNode;
                _startNode = newNode;
            }

            SingleNode<T> currentNode = _startNode;
            while (currentNode.Next != node)
            {
                currentNode = currentNode.Next;
                if (currentNode.Next == null)
                {
                    throw new Exception("Can not find node");
                }
            }
            newNode.Next = currentNode.Next;
            currentNode.Next = newNode;
        }

        public void AddFirst(T value)
        {
            SingleNode<T> newNode = new SingleNode<T>(value);
            newNode.Next = _startNode;
            _startNode = newNode;
        }

        public void AddLast(T value)
        {
            SingleNode<T> newNode = new SingleNode<T>(value);
            SingleNode<T> lastNode = GetLastNode();

            if (lastNode == null)
            {
                _startNode = newNode;
            }
            else
            {
                lastNode.Next = newNode;
            }

        }

        public void RemoveAll(T value)
        {
            if (_startNode == null)
            {
                return;
            }

            while (_startNode.Value.Equals(value))
            {
                _startNode = _startNode.Next;

                if (_startNode == null)
                {
                    return;
                }
            }

            SingleNode<T> previousNode = _startNode;
            SingleNode<T> currentNode = _startNode.Next;
            while (currentNode != null)
            {
                if (currentNode.Value.Equals(value))
                {
                    previousNode.Next = currentNode.Next;
                    currentNode.Next = null;
                    currentNode = previousNode;
                }
                previousNode = currentNode;
                currentNode = currentNode.Next;
            }
        }

        public void RemoveFirst()
        {
            if (_startNode == null)
            {
                return;
            }

            _startNode = _startNode.Next;
        }

        public void RemoveLast()
        {

            if (_startNode == null)
            {
                return;
            }

            if (_startNode.Next == null)
            {
                _startNode = null;
                return;
            }

            SingleNode<T> currentNode = _startNode;
            while (currentNode.Next.Next != null)
            {
                currentNode = currentNode.Next;
            }

            currentNode.Next = null;
        }

        public int Count()
        {
            int count = 0;
            SingleNode<T> currentNode = _startNode;

            while (currentNode != null)
            {
                count++;
                currentNode = currentNode.Next;
            }

            return count;
        }

        public SingleNode<T> FindNode(T value)
        {
            SingleNode<T> currentNode = _startNode;
            while (currentNode != null)
            {
                if (currentNode.Value.Equals(value))
                {
                    return currentNode;
                }
                currentNode = currentNode.Next;
            }

            return null;
        }

        public SingleNode<T> GetFirstNode()
        {
            return _startNode;
        }

        public SingleNode<T> GetLastNode()
        {
            SingleNode<T> currentNode = _startNode;
            while (currentNode != null && currentNode.Next != null)
            {
                currentNode = currentNode.Next;
            }
            return currentNode;
        }

        public void Reverse()
        {
            SingleNode<T> previousNode = null;
            SingleNode<T> currentNode = _startNode;
            SingleNode<T> nextNode = null;

            while (currentNode != null)
            {
                nextNode = currentNode.Next;
                currentNode.Next = previousNode;
                previousNode = currentNode;
                currentNode = nextNode;
            }

            _startNode = previousNode;
        }

        public void RecursiveReverse()
        {
            _startNode = RecursiveReverse(_startNode);
        }

        private SingleNode<T> RecursiveReverse(SingleNode<T> currentNode)
        {
            if (currentNode == null || currentNode.Next == null)
            {
                return currentNode;
            }

            var head = RecursiveReverse(currentNode.Next);

            var nextNode = currentNode.Next;
            nextNode.Next = currentNode;
            // or currentNode.Next.Next = currentNode;

            currentNode.Next = null;

            return head;
        }

        public bool HasLoop()
        {
            SingleNode<T> pointerA = _startNode;
            SingleNode<T> pointerB = _startNode.Next;

            while (pointerB != null)
            {
                if (pointerA == pointerB)
                {
                    return true;
                }

                pointerA = pointerA.Next;
                pointerB = pointerB.Next?.Next;
            }

            return false;
        }

        public bool IsEqualWith(SingleLinkedListCustom<T> otherLinkedList)
        {
            var currentNode = _startNode;
            var otherCurrentNode = otherLinkedList._startNode;

            while (currentNode != null && otherCurrentNode != null)
            {
                if (currentNode.Value.Equals(otherCurrentNode.Value) == false)
                {
                    return false;
                }

                currentNode = currentNode.Next;
                otherCurrentNode = otherCurrentNode.Next;
            }

            if (currentNode == null && otherCurrentNode == null)
            {
                return true;
            }

            return false;
        }

    }
}
