using NUnit.Framework;
using System.Collections.Generic;

namespace Tests
{
    public class BuiltInLinkedListTests
    {
        private const string a = "a";
        private const string b = "b";
        private const string c = "c";
        private const string d = "d";
      
        [Test]
        public void Add_First_Node()
        {            
            LinkedList<string> linkedList = new LinkedList<string>();
            linkedList.AddFirst(a);
            linkedList.AddFirst(b);
            linkedList.AddFirst(c);
            
            Assert.AreEqual(c, linkedList.First.Value);
            Assert.AreEqual(a, linkedList.Last.Value);
        }

        [Test]
        public void Add_Last_Node()
        {
            LinkedList<string> linkedList = new LinkedList<string>();
            linkedList.AddLast(a);
            linkedList.AddLast(b);
            linkedList.AddLast(c);
             
            Assert.AreEqual(a, linkedList.First.Value);
            Assert.AreEqual(c, linkedList.Last.Value);
        }

        [Test]
        public void Add_After_Node()
        {
            LinkedList<string> linkedList = new LinkedList<string>();
            linkedList.AddLast(a);
            linkedList.AddLast(b);
            linkedList.AddLast(c);
            linkedList.AddAfter(linkedList.First, d);

            Assert.AreEqual(d, linkedList.First.Next.Value);
        }

        [Test]
        public void Add_Before_Node()
        {
            LinkedList<string> linkedList = new LinkedList<string>();
            linkedList.AddLast(a);
            linkedList.AddLast(b);
            linkedList.AddLast(c);
            linkedList.AddBefore(linkedList.First, d);

            Assert.AreEqual(d, linkedList.First.Value);
        }

        [Test]
        public void Remove_First_Node()
        {
            LinkedList<string> linkedList = new LinkedList<string>();

            linkedList.AddLast(a);
            linkedList.RemoveFirst();
            Assert.IsNull(linkedList.First);

            linkedList.AddLast(a);
            linkedList.AddLast(b);
            linkedList.AddLast(c);
            linkedList.RemoveFirst();
            Assert.AreEqual(b, linkedList.First.Value);
        }

        [Test]
        public void Remove_Node_With_Value()
        {
            LinkedList<string> linkedList = new LinkedList<string>();
            linkedList.AddLast(a);
            linkedList.Remove(a);
            Assert.IsNull(linkedList.First);
            
            linkedList.AddLast(a);
            linkedList.AddLast(b);
            linkedList.AddLast(c);
            linkedList.Remove(c);
            Assert.AreEqual(b, linkedList.Last.Value);
        }

        [Test]
        public void Remove_Last_Node()
        {
            LinkedList<string> linkedList = new LinkedList<string>();
            linkedList.AddLast(a);
            linkedList.RemoveLast();
            Assert.IsNull(linkedList.Last);

            linkedList.AddLast(a);
            linkedList.AddLast(b);
            linkedList.AddLast(c);
            linkedList.RemoveLast();
            Assert.AreEqual(b, linkedList.Last.Value);
        }

        [Test]
        public void Get_First_Node()
        {
            LinkedList<string> linkedList = new LinkedList<string>();
            Assert.IsNull(linkedList.First);

            linkedList.AddFirst(a);
            linkedList.AddLast(b);
            Assert.AreEqual(a, linkedList.First.Value);
        }

        [Test]
        public void Get_Last_Node()
        {
            LinkedList<string> linkedList = new LinkedList<string>();

            Assert.IsNull(linkedList.Last);

            linkedList.AddFirst(a);
            linkedList.AddLast(b);
            Assert.AreEqual(b, linkedList.Last.Value);
        }

        [Test]
        public void Get_Previous_Node()
        {
            LinkedList<string> linkedList = new LinkedList<string>();
            linkedList.AddLast(a);
            linkedList.AddLast(b);
            linkedList.AddLast(c);

            var previousNode = linkedList.Last.Previous;

            Assert.AreEqual(b, previousNode.Value);
        }

        [Test]
        public void Count_Nodes()
        {
            LinkedList<string> linkedList = new LinkedList<string>();

            Assert.AreEqual(0, linkedList.Count);
            linkedList.AddFirst(a);
            Assert.AreEqual(1, linkedList.Count);
            linkedList.AddFirst(b);
            Assert.AreEqual(2, linkedList.Count);
        }

    }
}