using LinkedListExample;
using NUnit.Framework;
using System.Collections.Generic;

namespace Tests
{
    public class CustomSingleLinkedListTests
    {
        private const string a = "a";
        private const string b = "b";
        private const string c = "c";
        private const string d = "d";
        private const string e = "e";

        [Test]
        public void Add_After_Node()
        {
            SingleLinkedListCustom<string> linkedList = new SingleLinkedListCustom<string>();
            linkedList.AddLast(a);
            linkedList.AddLast(b);
            linkedList.AddLast(c);
            linkedList.AddAfter(linkedList.GetFirstNode(), d);

            Assert.AreEqual(d, linkedList.GetFirstNode().Next.Value);
        }

        [Test]
        public void Add_Before_Node()
        {
            SingleLinkedListCustom<string> linkedList = new SingleLinkedListCustom<string>();
            linkedList.AddLast(a);
            linkedList.AddLast(b);
            linkedList.AddLast(c);
            linkedList.AddBefore(linkedList.GetFirstNode(), d);

            Assert.AreEqual(d, linkedList.GetFirstNode().Value);
        }

        [Test]
        public void Add_First_Node()
        {
            SingleLinkedListCustom<string> linkedList = new SingleLinkedListCustom<string>();
            linkedList.AddFirst(a);
            linkedList.AddFirst(b);
            linkedList.AddFirst(c);

            Assert.AreEqual(c, linkedList.GetFirstNode().Value);
            Assert.AreEqual(a, linkedList.GetLastNode().Value);
        }

        [Test]
        public void Add_Last_Node()
        {
            SingleLinkedListCustom<string> linkedList = new SingleLinkedListCustom<string>();
            linkedList.AddLast(a);
            linkedList.AddLast(b);
            linkedList.AddLast(c);

            Assert.AreEqual(a, linkedList.GetFirstNode().Value);
            Assert.AreEqual(c, linkedList.GetLastNode().Value);
        }

        [Test]
        public void Remove_First_Node()
        {
            SingleLinkedListCustom<string> linkedList = new SingleLinkedListCustom<string>();

            linkedList.AddLast(a);
            linkedList.RemoveFirst();
            Assert.IsNull(linkedList.GetFirstNode());

            linkedList.AddLast(a);
            linkedList.AddLast(b);
            linkedList.AddLast(c);
            linkedList.RemoveFirst();
            Assert.AreEqual(b, linkedList.GetFirstNode().Value);
        }

        [Test]
        public void RemoveAll_When_List_Empty_Do_Nothing()
        {
            SingleLinkedListCustom<string> linkedList = new SingleLinkedListCustom<string>();
            linkedList.RemoveAll(a);
            Assert.IsNull(linkedList.GetFirstNode());
        }

        [Test]
        public void RemoveAll_When_Only_One_Node_Empty_List()
        {
            SingleLinkedListCustom<string> linkedList = new SingleLinkedListCustom<string>();
            linkedList.AddLast(a);
            linkedList.RemoveAll(a);
            Assert.IsNull(linkedList.GetFirstNode());
        }

            [Test]
        public void RemoveAll_Nodes_When_All_Nodes_Have_Value_Empty_List()
        {
            SingleLinkedListCustom<string> linkedList = new SingleLinkedListCustom<string>();
            linkedList.AddLast(a);
            linkedList.AddLast(a);
            linkedList.AddLast(a);
            linkedList.RemoveAll(a);
            Assert.IsNull(linkedList.GetFirstNode());
        }

        [Test]
        public void RemoveAll_Nodes_With_Value()
        {
            SingleLinkedListCustom<string> linkedList = new SingleLinkedListCustom<string>();
            linkedList.AddLast(c);
            linkedList.AddLast(a);
            linkedList.AddLast(b);
            linkedList.AddLast(c);
            linkedList.AddLast(c);
            linkedList.RemoveAll(c);
            Assert.AreEqual(a, linkedList.GetFirstNode().Value);
            Assert.AreEqual(b, linkedList.GetLastNode().Value);
        }

        [Test]
        public void Remove_Last_Node()
        {
            SingleLinkedListCustom<string> linkedList = new SingleLinkedListCustom<string>();
            linkedList.AddLast(a);
            linkedList.RemoveLast();
            Assert.IsNull(linkedList.GetLastNode());

            linkedList.AddLast(a);
            linkedList.AddLast(b);
            linkedList.AddLast(c);
            linkedList.RemoveLast();
            Assert.AreEqual(b, linkedList.GetLastNode().Value);
        }

        [Test]
        public void Get_First_Node()
        {
            SingleLinkedListCustom<string> linkedList = new SingleLinkedListCustom<string>();
            Assert.IsNull(linkedList.GetFirstNode());

            linkedList.AddFirst(a);
            linkedList.AddLast(b);
            Assert.AreEqual(a, linkedList.GetFirstNode().Value);
        }

        [Test]
        public void Get_Last_Node()
        {
            SingleLinkedListCustom<string> linkedList = new SingleLinkedListCustom<string>();
            Assert.IsNull(linkedList.GetLastNode());

            linkedList.AddFirst(a);
            linkedList.AddLast(b);
            Assert.AreEqual(b, linkedList.GetLastNode().Value);
        }
        
        [Test]
        public void Count_Nodes()
        {
            LinkedList<string> linkedList = new LinkedList<string>();

            Assert.AreEqual(0, linkedList.Count);
            linkedList.AddFirst(a);
            Assert.AreEqual(1, linkedList.Count);
            linkedList.AddFirst(b);
            Assert.AreEqual(2, linkedList.Count);
        }

        [Test]
        public void ReverseList()
        {
            SingleLinkedListCustom<string> linkedList = new SingleLinkedListCustom<string>();
            linkedList.AddLast(a);
            linkedList.AddLast(b);
            linkedList.AddLast(c);
            linkedList.AddLast(d);
            linkedList.AddLast(e);

            linkedList.Reverse();

            Assert.AreEqual(e, linkedList.GetFirstNode().Value);
            Assert.AreEqual(a, linkedList.GetLastNode().Value);
        }

        [Test]
        public void RecursiveReverseList()
        {
            SingleLinkedListCustom<string> linkedList = new SingleLinkedListCustom<string>();
            linkedList.AddLast(a);
            linkedList.AddLast(b);
            linkedList.AddLast(c);

            linkedList.RecursiveReverse();

            Assert.AreEqual(c, linkedList.GetFirstNode().Value);
            Assert.AreEqual(a, linkedList.GetLastNode().Value);
        }

        [Test]
        public void HasLoop_Returns_True_WhenLoopExists()
        {
            SingleLinkedListCustom<string> linkedList = new SingleLinkedListCustom<string>();
            linkedList.AddLast(a);
            linkedList.AddLast(b);
            linkedList.AddLast(c);
            linkedList.AddLast(d);
            linkedList.AddLast(e);
            var lastnode = linkedList.GetLastNode();
            lastnode.Next = linkedList.FindNode(b);

            var hasLoop = linkedList.HasLoop();

            Assert.IsTrue(hasLoop);
        }

        [Test]
        public void HasLoop_Returns_False_WhenNoLoopExists()
        {
            SingleLinkedListCustom<string> linkedList = new SingleLinkedListCustom<string>();
            linkedList.AddLast(a);
            linkedList.AddLast(b);
            linkedList.AddLast(c);
            linkedList.AddLast(d);

            var hasLoop = linkedList.HasLoop();

            Assert.IsFalse(hasLoop);
        }

        [Test]
        public void IsEqualWith_Returns_True_WhenListsAreTheSame()
        {
            SingleLinkedListCustom<string> linkedList1 = new SingleLinkedListCustom<string>();
            linkedList1.AddLast(a);
            linkedList1.AddLast(b);
            linkedList1.AddLast(c);
            linkedList1.AddLast(d);

            SingleLinkedListCustom<string> linkedList2 = new SingleLinkedListCustom<string>();
            linkedList2.AddLast(a);
            linkedList2.AddLast(b);
            linkedList2.AddLast(c);
            linkedList2.AddLast(d);

            var isEqual = linkedList1.IsEqualWith(linkedList2);

            Assert.IsTrue(isEqual);
        }

        [Test]
        public void IsEqualWith_Returns_False_WhenListsAreNotTheSame()
        {
            SingleLinkedListCustom<string> linkedList1 = new SingleLinkedListCustom<string>();
            linkedList1.AddLast(a);
            linkedList1.AddLast(b);
            linkedList1.AddLast(c);
            linkedList1.AddLast(d);

            SingleLinkedListCustom<string> linkedList2 = new SingleLinkedListCustom<string>();
            linkedList2.AddLast(a);
            linkedList2.AddLast(a);
            linkedList2.AddLast(a);
            linkedList2.AddLast(a);

            var isEqual = linkedList1.IsEqualWith(linkedList2);

            Assert.IsFalse(isEqual);
        }

        [Test]
        public void IsEqualWith_Returns_False_WhenDifferentNodeCount()
        {
            SingleLinkedListCustom<string> linkedList1 = new SingleLinkedListCustom<string>();
            linkedList1.AddLast(a);
            linkedList1.AddLast(b);
            linkedList1.AddLast(c);
            linkedList1.AddLast(d);

            SingleLinkedListCustom<string> linkedList2 = new SingleLinkedListCustom<string>();
            linkedList2.AddLast(a);

            var isEqual = linkedList1.IsEqualWith(linkedList2);

            Assert.IsFalse(isEqual);
        }
    }
}