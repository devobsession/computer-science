using LinkedListExample;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Tests
{
    public class IntLinkedListTests
    {
        [Test]
        public void Construct_IntLinkedList_WithIntArray()
        {
            var values = new int[] { 1, 2, 3, 4, 5 };
            IntLinkedList intLinkedList = new IntLinkedList(values);

            int count = 0;
            foreach (var value in intLinkedList.GetValues())
            {
                Assert.AreEqual(values[count], value);

                count++;
            }

        }

        [Test]
        public void SortByValueSwap_IntLinkedList()
        {
            var values = new int[] { 3, 1, 5, 2, 4 };
            IntLinkedList intLinkedList = new IntLinkedList(values);

            intLinkedList.SortByValueSwap();
            
            int count = 0;
            Array.Sort(values);
            foreach (var value in intLinkedList.GetValues())
            {
                Assert.AreEqual(values[count], value);

                count++;
            }
        }

        [Test]
        public void SortByLinkSwap_IntLinkedList()
        {
            var values = new int[] { 3, 1, 5, 2, 4 };
            IntLinkedList intLinkedList = new IntLinkedList(values);

            intLinkedList.SortByLinkSwap();

            int count = 0;
            Array.Sort(values);
            foreach (var value in intLinkedList.GetValues())
            {
                Assert.AreEqual(values[count], value);

                count++;
            }
        }

        [Test]
        public void SortAndMerge_IntLinkedList()
        {
            var unsortedValues1 = new int[] { 4, 5, 6 };
            var unsortedValues2 = new int[] { 1, 2, 10 };
            IntLinkedList intLinkedList = new IntLinkedList(unsortedValues1);
            IntLinkedList otherIntLinkedList = new IntLinkedList(unsortedValues2);

            intLinkedList.SortAndMerge(otherIntLinkedList);

            int count = 0;

            var expectedValues = MergeAndSortArrays(unsortedValues1, unsortedValues2);
            foreach (var value in intLinkedList.GetValues())
            {
                Assert.AreEqual(expectedValues[count], value);

                count++;
            }
        }

        [Test]
        public void SortAndMerge_IntLinkedList_WhenDifferentLengths()
        {
            var unsortedValues1 = new int[] { 1, 3, 5 };
            var unsortedValues2 = new int[] { 2, 4, 6, 7, 8, 9 };
            
            IntLinkedList intLinkedList = new IntLinkedList(unsortedValues1);
            IntLinkedList otherIntLinkedList = new IntLinkedList(unsortedValues2);

            intLinkedList.SortAndMerge(otherIntLinkedList);

            int count = 0;

            var expectedValues = MergeAndSortArrays(unsortedValues1, unsortedValues2);
            foreach (var value in intLinkedList.GetValues())
            {
                Assert.AreEqual(expectedValues[count], value);

                count++;
            }
        }

        [Test]
        public void SortAndMerge_IntLinkedList_WhenHasRepeatNumbers()
        {
            var unsortedValues1 = new int[] { 4, 5, 5, 5, 5, 6 };
            var unsortedValues2 = new int[] { 1, 2, 10 };

            IntLinkedList intLinkedList = new IntLinkedList(unsortedValues1);
            IntLinkedList otherIntLinkedList = new IntLinkedList(unsortedValues2);

            intLinkedList.SortAndMerge(otherIntLinkedList);

            int count = 0;
            var expectedValues = MergeAndSortArrays(unsortedValues1, unsortedValues2);
            foreach (var value in intLinkedList.GetValues())
            {
                Assert.AreEqual(expectedValues[count], value);

                count++;
            }
        }


        private int[] MergeAndSortArrays(int[] a, int[] b)
        {
            int[] newArray = new int[a.Length + b.Length];
            Array.Copy(a, newArray, a.Length);
            Array.Copy(b, 0, newArray, a.Length, b.Length);

            Array.Sort(newArray);

            return newArray;
        }
    }
}