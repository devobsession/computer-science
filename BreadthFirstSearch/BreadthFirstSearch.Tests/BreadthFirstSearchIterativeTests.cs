using BreadthFirstSearch;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace BreadthFirstSearchIterativeTests
{
    public class BreadthFirstSearchIterativeTests
    {

        private BiNode<string> _root;

        [SetUp]
        public void Setup()
        {
            _root = new BiNode<string>(new NodeData<string>(0, "a"))
            {
                LeftNode = new BiNode<string>(new NodeData<string>(1, "b"))
                {
                    LeftNode = new BiNode<string>(new NodeData<string>(2, "c")),
                    RightNode = new BiNode<string>(new NodeData<string>(3, "d"))
                },
                RightNode = new BiNode<string>(new NodeData<string>(4, "e"))
                {
                    LeftNode = new BiNode<string>(new NodeData<string>(5, "f")),
                    RightNode = new BiNode<string>(new NodeData<string>(6, "g"))
                }
            };
        }

        [Test]
        public void BreadthFirstSearch_LevelOrder_Iterative_Correct()
        {                
            BreadthFirstSearchIterative breadthFirstSearchIterative = new BreadthFirstSearchIterative();
            var nodes = breadthFirstSearchIterative.LevelOrderTraversal(_root);
            
            int[] correctLevelorder = new int[] { 0, 1, 4, 2, 3, 5, 6 };
            int index = 0;
            foreach (var node in nodes)
            {
                Assert.AreEqual(correctLevelorder[index], node.Id);
                index++;
            }
        }

    }
}