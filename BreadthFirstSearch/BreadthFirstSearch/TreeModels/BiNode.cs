﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BreadthFirstSearch
{
    public class BiNode<T>
    {
        public BiNode<T> LeftNode { get; set; }
        public BiNode<T> RightNode { get; set; }
        public NodeData<T> Data { get; set; }

        public BiNode(NodeData<T> data)
        {
            Data = data;
        }
    }
}
