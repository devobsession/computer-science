﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BreadthFirstSearch
{
    public class BreadthFirstSearchIterative
    {
        public IEnumerable<NodeData<T>> LevelOrderTraversal<T>(BiNode<T> root)
        {
            Queue<BiNode<T>> queue = new Queue<BiNode<T>>();

            if(root != null)
            {
                yield return root.Data;
                queue.Enqueue(root);
            }

            BiNode<T> currentNode;
            while(queue.Count > 0)
            {
                currentNode = queue.Dequeue();

                if(currentNode.LeftNode != null)
                {
                    yield return currentNode.LeftNode.Data;
                    queue.Enqueue(currentNode.LeftNode);
                }

                if(currentNode.RightNode != null)
                {
                    yield return currentNode.RightNode.Data;
                    queue.Enqueue(currentNode.RightNode);
                }
            }
        }

    }
}
