﻿using System;
using System.Linq;

namespace BreadthFirstSearch
{
    public class BreadthFirstSearchRecursive
    {
        public void LevelOrderTraversal<T>(BiNode<T>[] nodes, Action<NodeData<T>[]> visit)
        {
            var nodeDataArray = nodes.Where(node => node != null).Select(node => node.Data).ToArray();
            visit(nodeDataArray);

            BiNode<T>[] nextNodes = new BiNode<T>[nodes.Length * 2];
            for(int i = 0; i < nodes.Length; i++)
            {
                nextNodes[i * 2] = nodes[i]?.LeftNode;
                nextNodes[(i * 2) + 1] = nodes[i]?.RightNode;
            }

            if(nextNodes.Any(x => x != null))
            {
                LevelOrderTraversal(nextNodes, visit);
            }
        }
    }
}
