﻿using System.Diagnostics;

namespace DepthFirstSearch
{
    public class TwoDArrayData<T>
    {
        public T[,] Arr { get; set; }
        public Coordinates Coordinates { get; set; }

        public TwoDArrayData(T[,] arr, Coordinates coordinates)
        {
            Arr = arr;
            Coordinates = coordinates;
        }
    }

    [DebuggerDisplay("X:{X} Y:{Y}")]
    public class Coordinates
    {
        public int X { get; set; }
        public int Y { get; set; }
        public object MetaData { get; set; }

        public Coordinates(int x, int y)
        {
            X = x;
            Y = y;
        }

        public bool IsInArray<T>(T[,] arr)
        {
            if (X < 0 || X > arr.GetLength(1)-1)
            {
                return false;
            }
            else if (Y < 0 || Y > arr.GetLength(0)-1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool operator ==(Coordinates obj1, Coordinates obj2)
        {
            if (obj1 is null && obj2 is null)
            {
                return true;
            }
            else if (obj1 is null || obj2 is null)
            {
                return false;
            }

            return obj1.X == obj2.X && obj1.Y == obj2.Y; ;
        }

        public static bool operator !=(Coordinates obj1, Coordinates obj2)
        {
            return !(obj1 == obj2);
        }

        public override bool Equals(object obj)
        {
            var comparingCoordinates = obj as Coordinates;
            if (comparingCoordinates is null)
            {
                return false;
            }

            return X == comparingCoordinates.X && Y == comparingCoordinates.Y;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = X.GetHashCode();
                hashCode = (hashCode * 397) ^ Y.GetHashCode();
                return hashCode;
            }
        }
    }
}
