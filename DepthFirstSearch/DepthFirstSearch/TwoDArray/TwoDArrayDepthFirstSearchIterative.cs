﻿using System;
using System.Collections.Generic;

namespace DepthFirstSearch
{
    public class TwoDArrayDepthFirstSearchIterative
    {
        public void Traversal<T>(T[,] arr, Coordinates root, Action<TwoDArrayData<T>> visit, Predicate<TwoDArrayData<T>> isVisited, Action<Coordinates, Coordinates> prePushToStack)
        {
            Stack<Coordinates> stack = new Stack<Coordinates>();
            stack.Push(root);
            while (stack.Count > 0)
            {
                Coordinates currentCoordinates = stack.Pop();
                visit(new TwoDArrayData<T>(arr, currentCoordinates));

                var top = new Coordinates(currentCoordinates.X, currentCoordinates.Y+1);
                if (top.IsInArray(arr) && isVisited(new TwoDArrayData<T>(arr, top)) == false)
                {
                    prePushToStack(currentCoordinates, top);
                    stack.Push(top);
                }

                var right = new Coordinates(currentCoordinates.X + 1, currentCoordinates.Y);
                if (right.IsInArray(arr) && isVisited(new TwoDArrayData<T>(arr, right)) == false)
                {
                    prePushToStack(currentCoordinates, right);
                    stack.Push(right);
                }

                var bottom = new Coordinates(currentCoordinates.X, currentCoordinates.Y - 1);
                if (bottom.IsInArray(arr) && isVisited(new TwoDArrayData<T>(arr, bottom)) == false)
                {
                    prePushToStack(currentCoordinates, bottom);
                    stack.Push(bottom);
                }

                var left = new Coordinates(currentCoordinates.X - 1, currentCoordinates.Y);
                if (left.IsInArray(arr) && isVisited(new TwoDArrayData<T>(arr, left)) == false)
                {
                    prePushToStack(currentCoordinates, left);
                    stack.Push(left);
                }
            }
        }

    }


}
