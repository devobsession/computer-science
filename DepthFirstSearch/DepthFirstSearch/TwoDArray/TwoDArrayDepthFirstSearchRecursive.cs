﻿using System;
using System.Collections.Generic;

namespace DepthFirstSearch
{
    public class TwoDArrayDepthFirstSearchRecursive
    {
        public void Traversal<T>(T[,] arr, Coordinates coordinates, Action<TwoDArrayData<T>> visit, Predicate<TwoDArrayData<T>> isVisited, Action<Coordinates, Coordinates> prePushToStack)
        {
            if(coordinates.Y < 0 || coordinates.Y > arr.GetLength(0)-1)
            {
                return;
            }

            if (coordinates.X < 0 || coordinates.X > arr.GetLength(1) - 1)
            {
                return;
            }

            if (isVisited(new TwoDArrayData<T>(arr, coordinates)))
            {
                return;
            }

            visit(new TwoDArrayData<T>(arr, coordinates));
           
            var top = new Coordinates(coordinates.X, coordinates.Y + 1);
            prePushToStack(coordinates, top);
            Traversal(arr, top, visit, isVisited, prePushToStack);
            
            var right = new Coordinates(coordinates.X + 1, coordinates.Y);
            prePushToStack(coordinates, right);
            Traversal(arr, right, visit, isVisited, prePushToStack);
            
            var bottom = new Coordinates(coordinates.X, coordinates.Y - 1);
            prePushToStack(coordinates, bottom);
            Traversal(arr, bottom, visit, isVisited, prePushToStack);
            
            var left = new Coordinates(coordinates.X - 1, coordinates.Y);
            prePushToStack(coordinates, left);
            Traversal(arr, left, visit, isVisited, prePushToStack);
        }
    }
}
