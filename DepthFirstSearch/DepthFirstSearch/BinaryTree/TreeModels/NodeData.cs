﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DepthFirstSearch
{
    public class NodeData<T>
    {
        public int Id { get; set; }
        public T Value { get; set; }

        public NodeData(int id, T value)
        {
            Value = value;
            Id = id;
        }
    }
}
