﻿using System;
using System.Collections.Generic;

namespace DepthFirstSearch
{
    public class BinaryTreeDepthFirstSearchIterative
    {
        public IEnumerable<NodeData<T>> InOrderTraversal<T>(BiNode<T> root)
        {
            HashSet<int> visitedNodeIds = new HashSet<int>();
            Stack<BiNode<T>> stack = new Stack<BiNode<T>>();
            stack.Push(root);
            BiNode<T> currentNode = root;

            while(stack.Count > 0)
            {
                currentNode = stack.Peek();
                if(currentNode.LeftNode != null && visitedNodeIds.Contains(currentNode.LeftNode.Data.Id) == false)
                {
                    stack.Push(currentNode.LeftNode);
                }
                else
                {
                    currentNode = stack.Pop();
                    visitedNodeIds.Add(currentNode.Data.Id);
                    yield return currentNode.Data;

                    if(currentNode.RightNode != null)
                    {
                        stack.Push(currentNode.RightNode);
                    }

                }
            }           
        }

        public IEnumerable<NodeData<T>> PreOrderTraversal<T>(BiNode<T> root)
        {
            HashSet<int> visitedNodeIds = new HashSet<int>();
            Stack<BiNode<T>> stack = new Stack<BiNode<T>>();
            stack.Push(root);
            BiNode<T> currentNode = null;

            while(stack.Count > 0)
            {
                currentNode = stack.Pop();
                visitedNodeIds.Add(currentNode.Data.Id);
                yield return currentNode.Data;

                if (currentNode.RightNode != null && visitedNodeIds.Contains(currentNode.RightNode.Data.Id) == false)
                {
                    stack.Push(currentNode.RightNode);
                }

                if(currentNode.LeftNode != null && visitedNodeIds.Contains(currentNode.LeftNode.Data.Id) == false)
                {
                    stack.Push(currentNode.LeftNode);
                }
            }
        }

        public IEnumerable<NodeData<T>> PostOrderTraversal<T>(BiNode<T> root)
        {
            HashSet<int> visitedNodeIds = new HashSet<int>();
            Stack<BiNode<T>> stack = new Stack<BiNode<T>>();
            stack.Push(root);
            BiNode<T> currentNode = null;

            while(stack.Count > 0)
            {
                currentNode = stack.Peek();
                if(currentNode.LeftNode != null && visitedNodeIds.Contains(currentNode.LeftNode.Data.Id) == false)
                {
                    stack.Push(currentNode.LeftNode);
                }
                else if(currentNode.RightNode != null && visitedNodeIds.Contains(currentNode.RightNode.Data.Id) == false)
                {
                    stack.Push(currentNode.RightNode);
                }
                else
                {
                    visitedNodeIds.Add(currentNode.Data.Id);
                    yield return currentNode.Data;
                    stack.Pop();
                }
            }
        }

    }


}
