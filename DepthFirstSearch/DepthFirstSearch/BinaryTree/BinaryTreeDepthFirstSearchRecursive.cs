﻿using System;
using System.Collections.Generic;

namespace DepthFirstSearch
{
    public class BinaryTreeDepthFirstSearchRecursive
    {
        public void InOrderTraversal<T>(BiNode<T> node, Action<NodeData<T>> visit)
        {
            if(node == null)
            {
                return;
            }

            InOrderTraversal(node.LeftNode, visit);
            
            visit(node.Data);

            InOrderTraversal(node.RightNode, visit);
        }

        public void PreOrderTraversal<T>(BiNode<T> node, Action<NodeData<T>> visit)
        {
            if(node == null)
            {
                return;
            }

            visit(node.Data);

            PreOrderTraversal(node.LeftNode, visit);

            PreOrderTraversal(node.RightNode, visit);
        }

        public void PostOrderTraversal<T>(BiNode<T> node, Action<NodeData<T>> visit)
        {
            if(node == null)
            {
                return;
            }

            PostOrderTraversal(node.LeftNode, visit);

            PostOrderTraversal(node.RightNode, visit);

            visit(node.Data);
        }
    }


}
