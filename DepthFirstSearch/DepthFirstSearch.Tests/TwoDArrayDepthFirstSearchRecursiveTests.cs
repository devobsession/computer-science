using DepthFirstSearch;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DepthFirstSearchTests
{
    public class TwoDArrayDepthFirstSearchRecursiveTests
    {
        private int[,] _arr;

        [SetUp]
        public void Setup()
        {
            _arr = new int[4, 4];
            _arr[0, 0] = 1;
            _arr[0, 1] = 1;
            _arr[0, 2] = 0;
            _arr[0, 3] = 0;

            _arr[1, 0] = 1;
            _arr[1, 1] = 1;
            _arr[1, 2] = 0;
            _arr[1, 3] = 1;

            _arr[2, 0] = 0;
            _arr[2, 1] = 0;
            _arr[2, 2] = 0;
            _arr[2, 3] = 1;

            _arr[3, 0] = 1;
            _arr[3, 1] = 1;
            _arr[3, 2] = 0;
            _arr[3, 3] = 0;
        }

        [Test]
        public void Number_Of_Islands()
        {
            NumberOfIslandsProblem numberOfIslandsProblem = new NumberOfIslandsProblem();
            TwoDArrayDepthFirstSearchRecursive twoDArrayDepthFirstSearchRecursive = new TwoDArrayDepthFirstSearchRecursive();
            
            var success = numberOfIslandsProblem.SolveSuccessful(twoDArrayDepthFirstSearchRecursive.Traversal);

            Assert.True(success);
        }

        [Test]
        public void Find_Path_Problem()
        {
            FindPathProblem findPathProblem = new FindPathProblem();
            TwoDArrayDepthFirstSearchRecursive twoDArrayDepthFirstSearchRecursive = new TwoDArrayDepthFirstSearchRecursive();
            
            var success = findPathProblem.SolveSuccessful(twoDArrayDepthFirstSearchRecursive.Traversal);

            Assert.True(success);
        }
    }
}