using DepthFirstSearch;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace DepthFirstSearchTests
{
    public class BinaryTreeDepthFirstSearchRecursiveTests
    {
        private BiNode<string> _root;

        [SetUp]
        public void Setup()
        {
            _root = new BiNode<string>(new NodeData<string>(0, "a"))
            {
                LeftNode = new BiNode<string>(new NodeData<string>(1, "b"))
                {
                    LeftNode = new BiNode<string>(new NodeData<string>(2, "c")),
                    RightNode = new BiNode<string>(new NodeData<string>(3, "d"))
                },
                RightNode = new BiNode<string>(new NodeData<string>(4, "e"))
                {
                    LeftNode = new BiNode<string>(new NodeData<string>(5, "f")),
                    RightNode = new BiNode<string>(new NodeData<string>(6, "g"))
                }
            };
        }

        [Test]
        public void DepthFirstSearch_PostOrder_Recursive_Correct()
        {
            BinaryTreeDepthFirstSearchRecursive depthFirstSearchRecursive = new BinaryTreeDepthFirstSearchRecursive();
            
            int index = 0;
            int[] _correctPostorder = new int[] { 2, 3, 1, 5, 6, 4, 0 };
            depthFirstSearchRecursive.PostOrderTraversal(_root, (nodeData) =>
            {
                Assert.AreEqual(_correctPostorder[index], nodeData.Id);
                index++;
            });
        }

        [Test]
        public void DepthFirstSearch_InOrder_Recursive_Correct()
        {
            BinaryTreeDepthFirstSearchRecursive depthFirstSearchRecursive = new BinaryTreeDepthFirstSearchRecursive();
            
            int index = 0;
            int[] correctInorder = new int[] { 2, 1, 3, 0, 5, 4, 6 };
            depthFirstSearchRecursive.InOrderTraversal(_root, (nodeData) =>
            {
                Assert.AreEqual(correctInorder[index], nodeData.Id);
                index++;
            });
        }

        [Test]
        public void DepthFirstSearch_PreOrder_Recursive_Correct()
        {
            BinaryTreeDepthFirstSearchRecursive depthFirstSearchRecursive = new BinaryTreeDepthFirstSearchRecursive();
            
            int index = 0;
            int[] correctPreorder = new int[] { 0, 1, 2, 3, 4, 5, 6 };
            depthFirstSearchRecursive.PreOrderTraversal(_root, (nodeData) =>
            {
                Assert.AreEqual(correctPreorder[index], nodeData.Id);
                index++;
            });
        }

    }
}