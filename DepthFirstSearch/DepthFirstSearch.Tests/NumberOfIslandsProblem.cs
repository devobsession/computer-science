using DepthFirstSearch;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DepthFirstSearchTests
{
    public class NumberOfIslandsProblem
    {
        public int[,] SetupArr()
        {
            int[,]  arr = new int[4, 4];
            arr[0, 0] = 1;
            arr[0, 1] = 1;
            arr[0, 2] = 0;
            arr[0, 3] = 0;

            arr[1, 0] = 1;
            arr[1, 1] = 1;
            arr[1, 2] = 0;
            arr[1, 3] = 1;

            arr[2, 0] = 0;
            arr[2, 1] = 0;
            arr[2, 2] = 0;
            arr[2, 3] = 1;

            arr[3, 0] = 1;
            arr[3, 1] = 1;
            arr[3, 2] = 0;
            arr[3, 3] = 0;

            return arr;
        }

        public bool SolveSuccessful(Action<int[,], Coordinates, Action<TwoDArrayData<int>>, Predicate<TwoDArrayData<int>>, Action<Coordinates, Coordinates>> dfs)
        {
            var arr = SetupArr();
            Action<TwoDArrayData<int>> visit = (data) => data.Arr[data.Coordinates.Y, data.Coordinates.X] = 0;
            Predicate<TwoDArrayData<int>> isVisited = (data) => data.Arr[data.Coordinates.Y, data.Coordinates.X] == 0;
            Action<Coordinates, Coordinates> prePushToStack = (curr, next) => { };
                      
            int islandCount = 0;
            for (int y = 0; y < arr.GetLength(0); y++)
            {
                for (int x = 0; x < arr.GetLength(1); x++)
                {
                    if (arr[y,x] == 1)
                    {
                        islandCount++;
                        dfs(arr, new Coordinates(x, y), visit, isVisited, prePushToStack);
                    }
                }
            }

            return islandCount == 3;
        }
    }
}