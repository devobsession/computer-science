using DepthFirstSearch;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DepthFirstSearchTests
{
    public class FindPathProblem
    {
        public int[,] SetupArr()
        {
            int[,]  arr = new int[4, 4];
            arr[0, 0] = 1;
            arr[0, 1] = 1;
            arr[0, 2] = 1;
            arr[0, 3] = 1;

            arr[1, 0] = 0;
            arr[1, 1] = 1;
            arr[1, 2] = 0;
            arr[1, 3] = 1;

            arr[2, 0] = 1;
            arr[2, 1] = 1;
            arr[2, 2] = 0;
            arr[2, 3] = 0;

            arr[3, 0] = 0;
            arr[3, 1] = 1;
            arr[3, 2] = 1;
            arr[3, 3] = 2;

            return arr;
        }

        public bool SolveSuccessful(Action<int[,], Coordinates, Action<TwoDArrayData<int>>, Predicate<TwoDArrayData<int>>, Action<Coordinates, Coordinates>> dfs)
        {
            var arr = SetupArr();
            List<Coordinates> path = new List<Coordinates>();
            Action<TwoDArrayData<int>> visit = (data) => 
            {
                if (data.Arr[data.Coordinates.Y, data.Coordinates.X] == 2)
                {
                    var coordinates = data.Coordinates;
                    while (coordinates != null)
                    {
                        path.Add(coordinates);
                        coordinates = coordinates.MetaData as Coordinates;
                    }
                    path.Reverse();
                }
                data.Arr[data.Coordinates.Y, data.Coordinates.X] = 0;
            };
            Predicate<TwoDArrayData<int>> isVisited = (data) => data.Arr[data.Coordinates.Y, data.Coordinates.X] == 0;
            Action<Coordinates, Coordinates> prePushToStack = (curr, next) => {
                next.MetaData = curr;
            };

            dfs(arr, new Coordinates(0, 0), visit, isVisited, prePushToStack);

            List<Coordinates> correctPath = new List<Coordinates>()
            {
                new Coordinates(0,0),
                new Coordinates(1,0),
                new Coordinates(1,1),
                new Coordinates(1,2),
                new Coordinates(1,3),
                new Coordinates(2,3),
                new Coordinates(3,3)
            };
            for (int i = 0; i < correctPath.Count; i++)
            {
                if (path[i] != correctPath[i])
                {
                    return false;
                }
            }
            
            return true;
        }
    }
}