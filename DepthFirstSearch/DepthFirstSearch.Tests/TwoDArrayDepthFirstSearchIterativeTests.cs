using DepthFirstSearch;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DepthFirstSearchTests
{
    public class TwoDArrayDepthFirstSearchIterativeTests
    {
        [Test]
        public void Number_Of_Islands()
        {
            NumberOfIslandsProblem numberOfIslandsProblem = new NumberOfIslandsProblem();
            TwoDArrayDepthFirstSearchIterative twoDArrayDepthFirstSearchIterative = new TwoDArrayDepthFirstSearchIterative();
            
            var success = numberOfIslandsProblem.SolveSuccessful(twoDArrayDepthFirstSearchIterative.Traversal);

            Assert.True(success);
        }

        [Test]
        public void Find_Path_Problem()
        {
            FindPathProblem findPathProblem = new FindPathProblem();
            TwoDArrayDepthFirstSearchIterative twoDArrayDepthFirstSearchIterative = new TwoDArrayDepthFirstSearchIterative();

            var success = findPathProblem.SolveSuccessful(twoDArrayDepthFirstSearchIterative.Traversal);

            Assert.True(success);
        }

    }
}