using DepthFirstSearch;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace DepthFirstSearchTests
{
    public class BinaryTreeDepthFirstSearchIterativeTests
    {
        private BiNode<string> _root;

        [SetUp]
        public void Setup()
        {
            _root = new BiNode<string>(new NodeData<string>(0, "a"))
            {
                LeftNode = new BiNode<string>(new NodeData<string>(1, "b"))
                {
                    LeftNode = new BiNode<string>(new NodeData<string>(2, "c")),
                    RightNode = new BiNode<string>(new NodeData<string>(3, "d"))
                },
                RightNode = new BiNode<string>(new NodeData<string>(4, "e"))
                {
                    LeftNode = new BiNode<string>(new NodeData<string>(5, "f")),
                    RightNode = new BiNode<string>(new NodeData<string>(6, "g"))
                }
            };
        }

        [Test]
        public void DepthFirstSearch_PostOrder_Iterative_Correct()
        {
            BinaryTreeDepthFirstSearchIterative depthFirstSearchIterative = new BinaryTreeDepthFirstSearchIterative();
            var nodes = depthFirstSearchIterative.PostOrderTraversal(_root);

            int[] correctPostorder = new int[] { 2, 3, 1, 5, 6, 4, 0 };
            int index = 0;
            foreach (var node in nodes)
            {
                Assert.AreEqual(correctPostorder[index], node.Id);
                index++;
            }
        }

        [Test]
        public void DepthFirstSearch_InOrder_Iterative_Correct()
        {
            BinaryTreeDepthFirstSearchIterative depthFirstSearchIterative = new BinaryTreeDepthFirstSearchIterative();
            var nodes = depthFirstSearchIterative.InOrderTraversal(_root);

            int[] correctInorder = new int[] { 2, 1, 3, 0, 5, 4, 6 };
            int index = 0;
            foreach (var node in nodes)
            {
                Assert.AreEqual(correctInorder[index], node.Id);
                index++;
            }
        }

        [Test]
        public void DepthFirstSearch_PreOrder_Iterative_Correct()
        {
            BinaryTreeDepthFirstSearchIterative depthFirstSearchIterative = new BinaryTreeDepthFirstSearchIterative();
            var nodes = depthFirstSearchIterative.PreOrderTraversal(_root);

            int[] correctPreorder = new int[] { 0, 1, 2, 3, 4, 5, 6 };
            int index = 0;
            foreach (var node in nodes)
            {
                Assert.AreEqual(correctPreorder[index], node.Id);
                index++;
            }
        }

    }
}