﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HashTableExample
{
    public class HashTableCustom<Tkey, Tvalue>
    {
        private LinkedList<KeyValuePair<Tkey, Tvalue>>[] buckets;

        public HashTableCustom(int size)
        {
            buckets = new LinkedList<KeyValuePair<Tkey, Tvalue>>[size];
        }

        public void Add(Tkey key, Tvalue value)
        {
            int bucket = GetBucket(key.GetHashCode());
                     
            if(buckets[bucket] == null)
            {
                buckets[bucket] = new LinkedList<KeyValuePair<Tkey, Tvalue>>();
            }
            else if(ContainsKey(key, bucket))
            {
                throw new ArgumentException("An item with the same key has already been added");
            }
            
            buckets[bucket].AddLast(new KeyValuePair<Tkey, Tvalue>(key, value));
        }

        public Tvalue GetValue(Tkey key)
        {
            int hash = GetBucket(key.GetHashCode());
            var bucket = buckets[hash];

            if(bucket != null)
            {
                foreach(var item in bucket)
                {
                    if(item.Key.Equals(key))
                    {
                        return item.Value;
                    }
                }
            }
            

            throw new KeyNotFoundException("The given key was not present");
        }

        public bool ContainsKey(Tkey key)
        {
            return ContainsKey(key, GetBucket(key.GetHashCode()));
        }

        private bool ContainsKey(Tkey key, int bucket)
        {
            if(buckets[bucket] != null)
            {
                foreach(var existingItem in buckets[bucket])
                {
                    if(existingItem.Key.Equals(key))
                    {
                        return true;
                    }                        
                }                    
            }                
            return false;
        }

        private int GetBucket(int hashcode) 
        {
            // unchecked keyword suppresses overflow-checking
            // int1 = 2147483647 + 10; // equals -2,147,483,639
            unchecked
            {
                // A Hash code can be negative. To make sure that you end up with a positive value cast the value to an unsigned int. 
                // By modding the hascode by the length you will always get a number between 0 and the length-1,
                // therefore an infinite amount of items can be divided into a set number of buckets without knowing what the items look like up front
                // equal items have equal hash codes. Note: with this type of hashing should not be used for uniquiness as there will be plenty of overlap.
                return (int)((uint)hashcode % (uint)buckets.Length);
            }
        }


    }
}
