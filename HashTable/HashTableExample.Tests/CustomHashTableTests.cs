using HashTableExample;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Tests
{
    public class CustomHashTableTests
    {
        [Test]
        public void HashTableCustom_Insert_And_Check_Contains_Inserted_Int()
        {
            HashTableCustom<string, string> hashTableCustom = new HashTableCustom<string, string>(100);
            hashTableCustom.Add("key 1", "value 1");
            bool containsOne = hashTableCustom.ContainsKey("key 1");

            Assert.IsTrue(containsOne);
        }

        [Test]
        public void HashTableCustom_Does_Not_Contain_Return_False()
        {
            HashTableCustom<string, string> hashTableCustom = new HashTableCustom<string, string>(100);
            bool containsOne = hashTableCustom.ContainsKey("key 1");

            Assert.IsFalse(containsOne);
        }

        [Test]
        public void HashTableCustom_Throws_Exception_For_Duplicated_Keys()
        {
            HashTableCustom<string, string> hashTableCustom = new HashTableCustom<string, string>(100);
            bool exceptionThrown = false;

            hashTableCustom.Add("key 1", "value 1");
            try
            {
                hashTableCustom.Add("key 1", "value 1");
            }
            catch(ArgumentException ex)
            {
                exceptionThrown = true;
            }

            Assert.IsTrue(exceptionThrown);
        }

        [Test]
        public void HashTableCustom_Insert_And_Get_Value()
        {
            HashTableCustom<string, string> hashTableCustom = new HashTableCustom<string, string>(100);
            hashTableCustom.Add("key 1", "value 1");

            var value = hashTableCustom.GetValue("key 1");
            Assert.AreEqual("value 1", value);
        }

        [Test]
        public void HashTableCustom_Throws_Exception_For_Value_Not_There()
        {
            HashTableCustom<string, string> hashTableCustom = new HashTableCustom<string, string>(100);
            bool exceptionThrown = false;

            try
            {
                var value = hashTableCustom.GetValue("key 1");
            }
            catch(KeyNotFoundException ex)
            {
                exceptionThrown = true;
            }

            Assert.IsTrue(exceptionThrown);
        }
    }
}