using HashTableExample;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Tests
{
    // Monos implementation is open source
    // https://github.com/mono/mono/blob/mono-3.0.3/mcs/class/corlib/System.Collections.Generic/Dictionary.cs

    public class BuiltInDictionaryTests
    {        
        [Test]
        public void Dictionary_Insert_And_Check_Contains_Inserted()
        {
            Dictionary<string, string> dictionaryBuiltIn = new Dictionary<string, string>();

            dictionaryBuiltIn.Add("key 1", "item 1");

            bool containsKey = dictionaryBuiltIn.ContainsKey("key 1");
            Assert.IsTrue(containsKey);

            bool containsValue = dictionaryBuiltIn.ContainsValue("item 1");
            Assert.IsTrue(containsValue);
        }

        [Test]
        public void Dictionary_Does_Not_Contain_Return_False()
        {
            Dictionary<string, string> dictionaryBuiltIn = new Dictionary<string, string>();
            bool containsKey = dictionaryBuiltIn.ContainsKey("key 2");

            Assert.IsFalse(containsKey);
        }

        [Test]
        public void Dictionary_Throws_Exception_For_Duplicated_Keys()
        {
            Dictionary<string, string> dictionaryBuiltIn = new Dictionary<string, string>();
            bool exceptionThrown = false;

            dictionaryBuiltIn.Add("key 1", "item 1");
            try
            {
                dictionaryBuiltIn.Add("key 1", "item 1");
            }
            catch(ArgumentException ex)
            {
                exceptionThrown = true;
            }

            Assert.IsTrue(exceptionThrown);
        }

        [Test]
        public void Dictionary_Insert_And_Get_Value()
        {
            Dictionary<string, string> dictionaryBuiltIn = new Dictionary<string, string>();
            dictionaryBuiltIn.Add("key 1", "value 1");

            var value = dictionaryBuiltIn["key 1"];
            Assert.AreEqual("value 1", value);
        }

        [Test]
        public void Dictionary_Throws_Exception_For_Value_Not_There()
        {
            Dictionary<string, string> dictionaryBuiltIn = new Dictionary<string, string>();
            bool exceptionThrown = false;
           
            try
            {
                var value = dictionaryBuiltIn["key 1"];
            }
            catch(KeyNotFoundException ex)
            {
                exceptionThrown = true;
            }

            Assert.IsTrue(exceptionThrown);
        }

    }
}