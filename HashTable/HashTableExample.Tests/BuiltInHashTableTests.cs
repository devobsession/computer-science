using NUnit.Framework;
using System;
using System.Collections;

namespace Tests
{
    // Monos implementation is open source
    // https://github.com/mono/mono/blob/mono-3.0.3/mcs/class/corlib/System.Collections/Hashtable.cs

    public class BuiltInHashTableTests
    {
        [Test]
        public void HashTable_Insert_And_Check_Contains_Inserted_Key()
        {
            Hashtable hashTableBuiltIn = new Hashtable();
            hashTableBuiltIn.Add("key 1", "item 1");

            bool containsKey = hashTableBuiltIn.ContainsKey("key 1");
            Assert.IsTrue(containsKey);

            bool containsValue = hashTableBuiltIn.ContainsValue("item 1");
            Assert.IsTrue(containsValue);
        }

        [Test]
        public void HashTable_Does_Not_Contain_Return_False()
        {
            Hashtable hashTableBuiltIn = new Hashtable();
            bool containsKey = hashTableBuiltIn.ContainsKey("key 1");
            Assert.IsFalse(containsKey);
        }

        [Test]
        public void HashTable_Throws_Exception_For_Duplicated_Keys()
        {
            Hashtable hashTableBuiltIn = new Hashtable();
            bool exceptionThrown = false;

            hashTableBuiltIn.Add("key 1", "item 1");
            try
            {
                hashTableBuiltIn.Add("key 1", "item 1");
            }
            catch(ArgumentException ex)
            {
                exceptionThrown = true;
            }


            Assert.IsTrue(exceptionThrown);
        }

        [Test]
        public void HashTable_Insert_And_Get_Value()
        {
            Hashtable hashTableBuiltIn = new Hashtable();
            hashTableBuiltIn.Add("key 1", "value 1");

            object value = hashTableBuiltIn["key 1"];
            Assert.AreEqual("value 1", value);
        }

        [Test]
        public void HashTable_Throws_Exception_For_Value_Not_There()
        {
            Hashtable hashTableBuiltIn = new Hashtable();
            object value = hashTableBuiltIn["key 1"];

            Assert.IsNull(value);
        }

    }
}