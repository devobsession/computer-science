﻿using System;

namespace StackExample
{
    public class StackLinkedListCustom<T>
    {
        private Node<T> _top;
              
        public void Push(T item)
        {
            Node<T> newNode = new Node<T>(item)
            {
                Next = _top
            };
            _top = newNode;
        }

        public T Pop()
        {
            if(IsEmpty())
            {
                throw new InvalidOperationException("Stack underflow");
            }

            Node<T> item = _top;
            _top = _top.Next;
            return item.Value;
        }
        
        public T Peek()
        {
            if(IsEmpty())
            {
                throw new InvalidOperationException("Stack underflow");
            }

            return _top.Value;
        }

        public bool IsEmpty()
        {
            if(_top == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
