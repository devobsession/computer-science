﻿using System;

namespace StackExample
{
    public class StackArrayCustom<T>
    {
        private T[] _stackArray;
        private int _top;

        public StackArrayCustom()
        {
            _stackArray = new T[10];
            _top = -1;
        }

        public StackArrayCustom(int size)
        {
            _stackArray = new T[size];
            _top = -1;
        }

        public void Push(T item)
        {
            _top++;
            if(IsFull())
            {
                throw new InvalidOperationException("Stack overflow");
            }
            _stackArray[_top] = item;
        }

        public T Pop()
        {
            if(IsEmpty())
            {
                throw new InvalidOperationException("Stack underflow");
            }
            T item = _stackArray[_top];
            _top--;
            return item;
        }

        public T Peek()
        {
            if(IsEmpty())
            {
                throw new InvalidOperationException("Stack underflow");
            }
            return _stackArray[_top];
        }

        public bool IsFull()
        {
            if(_top >= _stackArray.Length-1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsEmpty()
        {
            if(_top == - 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int Count()
        {
            return _top+1;
        }
    }
}
