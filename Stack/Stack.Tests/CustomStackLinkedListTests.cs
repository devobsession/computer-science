using NUnit.Framework;
using StackExample;

namespace Tests
{
    public class CustomStackLinkedListTests
    {
        [Test]
        public void Pop_Top_Of_Stack()
        {
            StackLinkedListCustom<int> stack = new StackLinkedListCustom<int>();
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);

            int item = stack.Pop();
            Assert.AreEqual(3, item);

            item = stack.Pop();
            Assert.AreEqual(2, item);
        }

        [Test]
        public void Peek_Top_Of_Stack()
        {
            StackLinkedListCustom<int> stack = new StackLinkedListCustom<int>();
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);

            int item = stack.Peek();
            Assert.AreEqual(3, item);

            item = stack.Peek();
            Assert.AreEqual(3, item);
        }

        [Test]
        public void UnderFlow()
        {
            Assert.Throws(typeof(System.InvalidOperationException), () => {
                StackLinkedListCustom<int> stack = new StackLinkedListCustom<int>();
                int item = stack.Pop();
            });
        }
    }
}