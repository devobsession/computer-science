using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

namespace Tests
{
    public class BuiltInStackTests
    {
        [Test]
        public void Pop_Top_Of_Stack()
        {
            Stack<int> stack = new Stack<int>();
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);

            int item = stack.Pop();
            Assert.AreEqual(3, item);

            item = stack.Pop();
            Assert.AreEqual(2, item);
        }

        [Test]
        public void Peek_Top_Of_Stack()
        {
            Stack<int> stack = new Stack<int>();
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);

            int item =  stack.Peek();
            Assert.AreEqual(3, item);

            item = stack.Peek();
            Assert.AreEqual(3, item);
        }

        [Test]
        public void UnderFlow()
        {            
            Assert.Throws(typeof(System.InvalidOperationException), () => {
                Stack<int> stack = new Stack<int>();
                int item = stack.Pop();
            });
        }

    }
}