﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArrayExample
{
    public class ArrayListCustom<T>
    {
        public int Length { get; private set; }
        public int Capacity { get; private set; }

        private T[] _arr;

        public ArrayListCustom()
        {
            Capacity = 0;
            Length = 0;
        }

        public ArrayListCustom(int initialSize)
        {
            _arr = new T[initialSize];
            Capacity = initialSize;
            Length = 0;
        }

        public void Add(T item)
        {
            if(_arr == null)
            {
                _arr = new T[4];
                Capacity = 4;
            }

            if(Length > Capacity - 1)
            {
                IncreaseArraySize();
            }

            _arr[Length] = item;
            Length++;
        }
        
        private void IncreaseArraySize()
        {
            int newSize = Capacity * 2;
            T[] newArr = new T[newSize];

            for(int i = 0; i < Capacity - 1; i++)
            {
                newArr[i] = _arr[i];
            }
            _arr = newArr;
            Capacity = newSize; 
        }

    }
}
