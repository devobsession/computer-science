﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ArrayExample.Test
{
    public class BuiltInArrayListTests
    {
        [Test]
        public void List_Extends()
        {
            List<int> list = new List<int>();
            Assert.AreEqual(0, list.Capacity);

            list.Add(1);
            Assert.AreEqual(4, list.Capacity);
            
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(5);
            Assert.AreEqual(8, list.Capacity);
        }

        [Test]
        public void List_Set_Initial_Size()
        {
            List<int> list = new List<int>(10);
            Assert.AreEqual(10, list.Capacity);

            list.Add(1);
            Assert.AreEqual(10, list.Capacity);

            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(5);
            Assert.AreEqual(10, list.Capacity);

            list.Add(6);
            list.Add(7);
            list.Add(8);
            list.Add(9);
            list.Add(10);
            list.Add(11);
            Assert.AreEqual(20, list.Capacity);
        }

        [Test]
        public void StringBuilder_Extends()
        {
            StringBuilder sb = new StringBuilder();
            Assert.AreEqual(16, sb.Capacity);

            sb.Append(1);
            sb.Append(2);
            sb.Append(3);
            sb.Append(4);
            sb.Append(5);
            sb.Append(6);
            sb.Append(7);
            sb.Append(8);
            sb.Append(9);
            sb.Append(10);
            sb.Append(11);
            sb.Append(12);
            sb.Append(13);
            sb.Append(14);
            sb.Append(15);
            sb.Append(16);
            sb.Append(17);
            Assert.AreEqual(32, sb.Capacity);

           
        }

        [Test]
        public void StringBuilder_Set_Initial_Size()
        {
            StringBuilder sb = new StringBuilder(10);
            Assert.AreEqual(10, sb.Capacity);

            sb.Append(1);
            Assert.AreEqual(10, sb.Capacity);

            sb.Append(2);
            sb.Append(3);
            sb.Append(4);
            sb.Append(5);
            Assert.AreEqual(10, sb.Capacity);

            sb.Append(6);
            sb.Append(7);
            sb.Append(8);
            sb.Append(9);
            sb.Append(10);
            sb.Append(11);
            Assert.AreEqual(20, sb.Capacity);
        }
        
        [Test]
        public void ArrayList_Extends()
        {
            ArrayList list = new ArrayList();
            Assert.AreEqual(0, list.Capacity);

            list.Add(1);
            Assert.AreEqual(4, list.Capacity);

            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(5);
            Assert.AreEqual(8, list.Capacity);
        }

        [Test]
        public void ArrayList_Set_Initial_Size()
        {
            ArrayList list = new ArrayList(10);
            Assert.AreEqual(10, list.Capacity);

            list.Add(1);
            Assert.AreEqual(10, list.Capacity);

            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(5);
            Assert.AreEqual(10, list.Capacity);

            list.Add(6);
            list.Add(7);
            list.Add(8);
            list.Add(9);
            list.Add(10);
            list.Add(11);
            Assert.AreEqual(20, list.Capacity);
        }

    }
}
