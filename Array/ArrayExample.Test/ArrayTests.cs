using NUnit.Framework;

namespace ArrayExample.Test
{
    public class ArrayTests
    {
        private readonly string[] _alphaBet = new string[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n" };

        [Test]
        public void One_D_Array()
        {
            string[] arr = new string[4];
            arr[0] = "a";
            arr[1] = "b";
            arr[2] = "c";
            arr[3] = "d";

            for(int i = 0; i < arr.Length; i++)
            {
                Assert.AreEqual(_alphaBet[i], arr[i]);
            }
        }

        [Test]
        public void Two_D_Array()
        {
            string[,] arr = new string[2,2];
            arr[0,0] = "a";
            arr[0,1] = "b";
            arr[1,0] = "c";
            arr[1,1] = "d";
            
            int levels = arr.Rank;
            Assert.AreEqual(2, levels);

            int count = 0;
            for(int x = 0; x < arr.GetLength(0); x++)
            {
                for(int y = 0; y < arr.GetLength(1); y++)
                {
                    Assert.AreEqual(_alphaBet[count], arr[x, y]);
                    count++;
                }                
            }
        }
        
        [Test]
        public void Three_D_Array()
        {
            string[,,] arr = new string[2, 2, 2];
            arr[0, 0, 0] = "a";
            arr[0, 0, 1] = "b";
            arr[0, 1, 0] = "c";
            arr[0, 1, 1] = "d";
            arr[1, 0, 0] = "e";
            arr[1, 0, 1] = "f";
            arr[1, 1, 0] = "g";
            arr[1, 1, 1] = "h";
            int levels = arr.Rank;
            Assert.AreEqual(3, levels);

            int count = 0;
            for(int x = 0; x < arr.GetLength(0); x++)
            {
                for(int y = 0; y < arr.GetLength(1); y++)
                {
                    for(int z = 0; z < arr.GetLength(2); z++)
                    {
                        Assert.AreEqual(_alphaBet[count], arr[x,y,z]);
                        count++;
                    }                    
                }
            }
        }
        
        [Test]
        public void Two_D_Array_Jaggered()
        {
            string[][] arr = new string[2][];

            arr[0] = new string[2];
            arr[0][0] = "a";
            arr[0][1] = "b";

            arr[1] = new string[3];
            arr[1][0] = "c";
            arr[1][1] = "d";
            arr[1][2] = "e";

            int levels = arr.Rank;
            Assert.AreEqual(1, levels);

            int count = 0;
            for(int x = 0; x < arr.Length; x++)
            {
                for(int y = 0; y < arr[x].Length; y++)
                {
                    Assert.AreEqual(_alphaBet[count], arr[x][y]);
                    count++;
                }
            }

        }

        [Test]
        public void Three_D_Array_Jaggered()
        {
            string[][][] arr = new string[2][][];

            arr[0] = new string[2][];
            arr[0][0] = new string[2];
            arr[0][0][0] = "a";
            arr[0][0][1] = "b";
            arr[0][1] = new string[3];
            arr[0][1][0] = "c";
            arr[0][1][1] = "d";
            arr[0][1][2] = "e";

            arr[1] = new string[3][];
            arr[1][0] = new string[2];
            arr[1][0][0] = "f";
            arr[1][0][1] = "g";
            arr[1][1] = new string[2];
            arr[1][1][0] = "h";
            arr[1][1][1] = "i";
            arr[1][2] = new string[3];
            arr[1][2][0] = "j";
            arr[1][2][1] = "k";
            arr[1][2][2] = "l";

            int levels = arr.Rank;
            Assert.AreEqual(1, levels);

            int count = 0;
            for(int x = 0; x < arr.Length; x++)
            {
                for(int y = 0; y < arr[x].Length; y++)
                {
                    for(int z = 0; z < arr[x][y].Length; z++)
                    {
                        Assert.AreEqual(_alphaBet[count], arr[x][y][z]);
                        count++;
                    }                    
                }
            }

        }

    }
}