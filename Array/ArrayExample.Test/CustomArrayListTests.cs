﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArrayExample.Test
{
    public class CustomArrayListTests
    {
        [Test]
        public void List_Extends()
        {
            ArrayListCustom<int> list = new ArrayListCustom<int>();
            Assert.AreEqual(0, list.Capacity);

            list.Add(1);
            Assert.AreEqual(4, list.Capacity);

            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(5);
            Assert.AreEqual(8, list.Capacity);
        }

        [Test]
        public void List_Set_Initial_Size()
        {
            List<int> list = new List<int>(10);
            Assert.AreEqual(10, list.Capacity);

            list.Add(1);
            Assert.AreEqual(10, list.Capacity);

            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(5);
            Assert.AreEqual(10, list.Capacity);

            list.Add(6);
            list.Add(7);
            list.Add(8);
            list.Add(9);
            list.Add(10);
            list.Add(11);
            Assert.AreEqual(20, list.Capacity);
        }
    }
}
