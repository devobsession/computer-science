﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsertionSort
{
    [DebuggerDisplay("{Id}")]
    public class Element<T>
    {
        public int Id { get; set; }
        public T Data { get; set; }

        public Element(int id, T data)
        {
            Data = data;
            Id = id;
        }
    }
}
