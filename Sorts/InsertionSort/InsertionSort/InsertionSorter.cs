﻿using System;

namespace InsertionSort
{
    public class InsertionSorter
    {
        public void Sort<T>(Element<T>[] arr)
        {
            Element<T> temp = null;
            int y = 0;

            for (int i = 1; i < arr.Length; i++)
            {
                temp = arr[i];
                for (y = i-1; y >= 0 && arr[y].Id > temp.Id; y--)
                {
                    arr[y+1] = arr[y];
                }

                arr[y+1] = temp;
            }
        }
    }
}
