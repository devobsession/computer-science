using NUnit.Framework;

namespace InsertionSort.UnitTest
{
    public class InsertionSortTests
    {
        private readonly int[] _correctIdOrder = new int[] { 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 7, 8, 9 };
        private readonly Element<string>[] _arr = new Element<string>[]
        {
            new Element<string>(8, "a"),
            new Element<string>(5, "a"),
            new Element<string>(2, "a"),
            new Element<string>(9, "a"),
            new Element<string>(4, "a"),
            new Element<string>(3, "a"),
            new Element<string>(6, "a"),
            new Element<string>(1, "a"),
            new Element<string>(7, "a"),
            new Element<string>(4, "b"),
            new Element<string>(2, "b"),
            new Element<string>(5, "b"),
            new Element<string>(1, "b"),
            new Element<string>(3, "b")
        };

        [Test]
        public void Sort_Orders_Correctly()
        {
            InsertionSorter insertionSort = new InsertionSorter();
            insertionSort.Sort(_arr);

            for (int i = 0; i < _arr.Length; i++)
            {
                Assert.AreEqual(_correctIdOrder[i], _arr[i].Id);
            }
        }

        [Test]
        public void Is_Stable_Sort()
        {
            InsertionSorter insertionSort = new InsertionSorter();
            insertionSort.Sort(_arr);

            bool isStable = true;

            for (int i = 0; i < 10; i += 2)
            {
                string dataA = _arr[i].Data;
                string dataB = _arr[i + 1].Data;

                if (!dataA.Equals("a") && !dataB.Equals("b"))
                {
                    isStable = false;
                    break;
                }
            }
            Assert.IsTrue(isStable);
        }
    }
}