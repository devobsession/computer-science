﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace SelectionSort
{
    [DebuggerDisplay("{Id}")]
    public class Element<T>
    {
        public int Id { get; set; }
        public T Data { get; set; }

        public Element(int id, T data)
        {
            Data = data;
            Id = id;
        }
    }
}
