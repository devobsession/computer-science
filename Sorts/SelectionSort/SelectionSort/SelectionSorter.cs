﻿using System;

namespace SelectionSort
{
    public class SelectionSorter
    {
        // Each pass select the smallest value and put in the next place
        // eg { 5, 9, 4, 6, 1, 7, 3, 0, 8, 2 }
        // Start the loop from the unsorted part of the array, starts at 0
        // loop through each element and find the smallest element and swap with the first index of the unsorted part of the array.
        // eg first pass swap index 7(0) with index 0(5)
        // eg second pass swap index 4(1) with index 1(9)
        // dont have to loop through last element
        // requires n-1 passes
        public void Sort<T>(Element<T>[] arr)
        {
            int smallestValuesIndex;

            for(int pointer = 0; pointer < arr.Length - 1; pointer++) // < arr.Length - 1 so that we do not loop through the last element
            {
                smallestValuesIndex = pointer;
                for(int i = pointer + 1; i < arr.Length; i++) // int j = i+1 so that we always start with the unsorted values.
                {
                    if(arr[i].Id < arr[smallestValuesIndex].Id)
                    {
                        smallestValuesIndex = i;
                    }
                }

                if(pointer != smallestValuesIndex)
                {
                    Swap(arr, pointer, smallestValuesIndex);
                }
            }
        }

        private void Swap<T>(Element<T>[] arr, int indexA, int indexB)
        {
            var tempValue = arr[indexA];
            arr[indexA] = arr[indexB];
            arr[indexB] = tempValue;
        }

        // Total comparisons n((n-1)/2)
        // O(n^2)

        // Not data sensitive: will run the same regardless of the order of the data
        // Low data movement: only one swap per iteration
        // In-place sort: the sorted items occupy the same storage as the original ones
        // Unstable sort: When a swap happens it may change the order of the duplicates

    }
}
