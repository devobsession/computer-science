﻿using System;

namespace BubbleSort
{
    public class BubbleSorter
    {
        // adjacent elements are compared and swapped if they are not in order.
        // Each pass compares current index with next index
        // eg { 5, 9, 4, 6, 1, 7, 3, 0, 8, 2 }
        // Each pass the largest element will move to the end of the array(bubble to the top), therefore each loop can decrease in size
        // eg compare index 0(val=5) and 1(val=9) and leave where they are
        // eg then compare index 1(val=9) and 2(val=4) and swap the values around
        // the 9 value will keep swaping until it reaches the last index

        public void Sort<T>(Element<T>[] arr)
        {
            bool hasSwapped;

            for(int pointer = arr.Length - 1; pointer > 0; pointer--)
            {
                hasSwapped = false;
                for(int i = 0; i < pointer; i++)
                {
                    if(arr[i].Id > arr[i + 1].Id)
                    {
                        Swap(arr, i, i + 1);
                        hasSwapped = true;
                    }
                }

                // If you have passed through the enitre set of data without swapping once then the data is sorted
                if(hasSwapped == false)
                {
                    break;
                }
            }
        }

        private void Swap<T>(Element<T>[] arr, int indexA, int indexB)
        {
            Element<T> tempvalue = arr[indexA];
            arr[indexA] = arr[indexB];
            arr[indexB] = tempvalue;
        }

  
        // O(n^2)
        // Is Data sensitive: the erlier the data is ordered then the earlier completes eg if data is already order then it wil only iteration once O(n) Worst case: reverse ordered list
        // High data movement: requires many data swaps per iteration
        // In-place sort: the sorted items occupy the same storage as the original ones
        // Stable sort: Duplicates remain in their original order
    }
}
