﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BubbleSort
{
    public class SingleLoopBubbleSorter
    {
     
        public void Sort<T>(Element<T>[] arr)
        {
            int elementsBubbledUp = 0;
            int i = 0;
            bool hasSwapped = false;

            while (i < arr.Length)
            {
           
                if(arr[i].Id > arr[i + 1].Id)
                {
                    Swap<T>(arr, i, i + 1);
                    hasSwapped = true;
                }
                if(i == arr.Length - 2 - elementsBubbledUp)
                {
                    if(hasSwapped == false)
                    {
                        break;
                    }

                    hasSwapped = false;
                    i = -1;
                    elementsBubbledUp++;
                }
                i++;
            }
        }

        private void Swap<T>(Element<T>[] arr, int indexA, int indexB)
        {
            Element<T> tempvalue = arr[indexA];
            arr[indexA] = arr[indexB];
            arr[indexB] = tempvalue;
        }

    }
}
