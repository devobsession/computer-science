﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BubbleSort
{
    public class SingleLoop2ArrayBubbleSorter
    {
        
        /// <summary>
        /// To sort two arrays using only a while loop and 3 variables
        /// </summary>
        /// <param name="arr1"></param>
        /// <param name="arr2"></param>
        public void Sort(int[] arr1, int[] arr2)
        {
            int temp = 0;
            int i = 0;
            int elementsBubbledUp = 0;

            while(true)
            {
                if(i < arr1.Length - 1 && arr1[i] > arr1[i + 1])
                {
                    temp = arr1[i];
                    arr1[i] = arr1[i + 1];
                    arr1[i + 1] = temp;
                }

                if(i < arr2.Length - 1 && arr2[i] > arr2[i + 1])
                {
                    temp = arr2[i];
                    arr2[i] = arr2[i + 1];
                    arr2[i + 1] = temp;
                }

                if(0 == (arr1.Length > arr2.Length ? arr1.Length : arr2.Length) - 2 - elementsBubbledUp)
                {
                    break;
                }

                if(i == (arr1.Length > arr2.Length ? arr1.Length : arr2.Length) - 2 - elementsBubbledUp)
                {

                    i = -1;
                    elementsBubbledUp++;
                }
                i++;
            }
        }
        
    }
}
