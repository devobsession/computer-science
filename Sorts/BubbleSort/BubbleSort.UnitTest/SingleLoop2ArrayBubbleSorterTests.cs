using NUnit.Framework;

namespace BubbleSort.UnitTest
{
    public class SingleLoop2ArrayBubbleSorterTests
    {
        private readonly int[] _correctIdOrder = new int[] { 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 7, 8, 9 };
        private readonly int[] _arr1 = new int[]
        {
            8,
            5,
            2,
            9,
            4,
            3,
            6,
            1,
            7,
            4,
            2,
            5,
            1,
            3
        };

        private readonly int[] _arr2 = new int[]
       {
            5,
            2,
            4,
            3,
            1,
            4,
            2,
            5,
            1,
            3
       };

        [Test]
        public void Sort_Orders_Correctly()
        {
            SingleLoop2ArrayBubbleSorter bubbleSorter = new SingleLoop2ArrayBubbleSorter();
            bubbleSorter.Sort(_arr1, _arr2);

            for(int i = 0; i < _arr1.Length; i++)
            {
                Assert.AreEqual(_correctIdOrder[i], _arr1[i]);
            }

            for (int i = 0; i < _arr2.Length; i++)
            {
                Assert.AreEqual(_correctIdOrder[i], _arr2[i]);
            }
        }

    }
}